# ENKI Cluster implementation procedures

For details and complete options see [Cluster Implementation Notes](#cluster-implementation-notes).

## Automatic Cluster Configuration
This method is utilized for the ENKI production cloud server.
- See README file for the repository [ENKI-portal/ENKI-cluster-config](https://gitlab.com/ENKI-portal/enki-cluster-config)
- The Docker image used by the ENKI-portal/ENKI-cluster-config project is generated automatically on a commit to the master branch of this project using CI commands in .gitlab-ci.yml and Cluster/Dockerfile.  Note that this image is publically accessible and can be run locally, as [described below](#running-the-docker-image-locally).  The generated image is named *enki-portal/thermoengine:master*. 
- If the commit pushed to this project is tagged, the Docker image is built and copied to *enki-portal/thermoengine:latest*. The CI/CD of ENKI-portal/ENKI-cluster-config is subsequently triggered and the production ENKI cluster image is updated to *enki-portal/thermoengine:latest*.
- See Settings -> Packages -> Container Registry for location and inventory of automatically generated Docker images. These Docker images are accessed as, e.g., *registry.gitlab.com/enki-portal/thermoengine:latest*.

## Manual Cluster Configuration
This method is suitable for deployment of a development or specialized workshop cluster. The configuration is accomplished on your Desktop computer utilizing a cloned version of a fork of this repository, which hereafter we will assume is also named ThermoEngine.

In what follow some familiarity with Google Cloud Engine and Docker image building is assumed.

- cd into the Cluster directory
- **[once only]** install Google cloud resources ("gcloud") on desktop machine
- **[once only]** install Docker on desktop machine
- Generate a docker image built on an existing JupyterHub chart
  - Create/update *Dockerfile*, see [notebook image source](https://github.com/jupyter/docker-stacks)
  - **[once only]** Authorize GitLab registry for container storage in ThermoEngine project area of GitLab website
  - In a terminal, login: ```docker login registry.gitlab.com``` using GitLab credentials. 
  - In a terminal (setting TAG to an image tag identifier of choice): 
    - ```docker build -t registry.gitlab.com/enki-portal/thermoengine/enki-portal:TAG .```
    - ```docker push registry.gitlab.com/enki-portal/thermoengine/enki-portal:TAG```
- Create Google Cloud resources using the shell script provided:
  - ```bash gcloud-1st.sh```
- Install Helm and Tiller using the shell script provided:
  - ```bash helm-2nd.sh```
  - Check that Helm is properly installed: ```helm version```
- Install initial configuration of JupyterHub on cluster using the shell script provided:
  - ```bash jupyterhub-3rd.sh``` 
  - Note that the stable version of the hub image (currently 0.8.2) is installed here.  This image will be updated to the latest development version later.
  - Note also that a secret token is defined here which must appear in all yaml files used to build and upgrade the cluster. A token may be generated using:
    - ```openssl rand -hex 32```
  - Find the IP address of the proxy server (execute repeatedly until one is assigned):
    - ```kubectl get service --namespace jhub```
  - At this stage the cluster permits non-secure login for a single user (*enki*) who has admin priviledges, with password *ENKI070719* 
- Update domain DNS (e.g., on your domain service provider) so that an *a-record* for your domain points to the generated IP address for the proxy server
- **[only once]** At your GitLab user profile web portal, generate an oauth token for a new application (enkiserver) that allows GitLab credentials to be used for cluster login.
  - Edit both files *config-upgrade.yaml* and *config-encrypt.yaml* to reflect oauth token credentials, your domain name, and the correct image TAG
- Upgrade the cluster to add https, read-only pull of ThermoEngine, and GitLab authentication using the shell script provided:
  - ```bash upgrade-4th.sh```
  - ```bash encrypt-5th.sh```
  - Note that these files currently upgrade to JupyterHub image 0.9.0-beta.4.n008.hb20ad22 which is the latest development version of the [Hub
](https://hub.docker.com/r/jupyterhub/jupyterhub/). Update this accordingly.

At this point the cluster should be running. 

To tear the cluster down use the shell script provided: ```bash teardown.sh```

Template shell files for configuring a workshop cluster may be found in the Cluster folder. These should be modified for the cluster region and configuration suitable for the workshop venue.

The shell scripts utilized in manual cluster configuration reference yaml files in the Cluster directory. Specify alternate JupyterHub configuration options by editing these files. 

### Manual configuration - Extant problems

- As of March 18, 2020. There is a bug in upgrading JupyterHub at step 4 (*upgrade-4th.sh*). Normally, the addition of https service in the referenced yaml file (*config-upgrade.yaml*) is included at this step, but unfortunately, this inclusion breaks the upgrade.  The workaround is to exclude the https addition and subsequently add it by running *encrypt-5th.sh*, which references *config-encrypt.yaml*. These two shell files are identical other than referenceing a different yaml config file. The *config-encrypt.yaml* file contains the letsencrypt entries that generate https service.  Otherise it is identical to *config-upgrade.yaml*. 

# Running the Docker image locally

This proceedure is patterned after that in the repository https://github.com/jupyterhub/jupyterhub-deploy-docker.git, as modified in the examples folder for Jupyter Lab deployment.   

- If you do not want to use the automatically generated Docker image at ENKI-portal/ThermoEngine, build the Docker image as described above in [Manual Cluster Configuration](#manual-cluster-configuration) and upload it to your Gitlab repository. It what follows it will be assumed that the latest automatically generated production image is being accessed.
- If Docker is not installed on your machine, do so.
- At a terminal on your local machine, login: ```docker login registry.gitlab.com``` using GitLab credentials.
- At a terminal on your local machine, run the command:
  - ```docker run -p 8888:8888 --env JUPYTER_ENABLE_LAB=yes --user root -e GRANT_SUDO=yes registry.gitlab.com/enki-portal/thermoengine:latest start-notebook.sh```
- This command will launch Jupyter Lab in the container. The user will have root permission with no password asked for sudo.  Access the notebook in a browser pointing to (or, the equivalent as printed to your terminal.):
  - ``` http://127.0.0.1:8888/?token=5193b5714271b44006edb619fb26cfd203cd113a998eff8b```
- Once you exit Jupyter Lab the container should exit, but if it doesn't kill the executing container with the terminal command:
  - ```docker kill $(docker ps -q)```
- See notes on other options for running the image at: 
  - https://jupyter-docker-stacks.readthedocs.io/en/latest/using/common.html
- An interactive shell into the container is obtained with:
  - ```docker run --user root -it --rm registry.gitlab.com/enki-portal/thermoengine:latest bash```

# Cluster implementation notes
**Important reference** URL: https://zero-to-jupyterhub.readthedocs.io/en/latest/

Below are some random configuration notes and option references, gathered in the process of developing shell, Docker and yaml scripts for configuring the ENKI cluster.  The material may be dated.

## Seting up a Kubernetes cluster
##### Preliminaries (once) ...
- Google Cloud (go to https://console.cloud.google.com/ and login in with cloud credentials)
- Once: Set up a cloud budget. Instructions: https://cloud.google.com/billing/docs/how-to/budgets
- Once: Enable Kubernetes Engine API (Instructions: https://console.cloud.google.com/apis/api/container.googleapis.com/overview). Subsequent, Kubernetes appears in left panel)
- Start *Google Cloud Shell* by clicking the left-most button in the block of buttons on the upper right of the menu bar.  This opens a web terminal and starts up a shell.  Type commands in the terminal.

##### Download Google Cloud SDK (once)
Allows working in a terminal on the local machine.  This is a preferred solution because then the configuration files like *config.yaml* can be stored locally and added to the repository.
```
curl https://sdk.cloud.google.com | bash
```
writes files into $HOME/google-cloud-sdk. Next, restart the shell:
```
exec -l $SHELL
```
Run gcloud init to initialize the gcloud environment:
```
gcloud init
```
Install kubectl. From your terminal, enter:
```
gcloud components install kubectl
```

##### Create a managed Kubernetes cluster and a default node pool
In a *Google Cloud Shell*:
```
gcloud container clusters create \
  --machine-type n1-standard-2 \
  --num-nodes 2 \
  --zone us-west1-a \
  --cluster-version latest \
  enkiserver
```
```
Output:
Creating cluster enkiserver in us-west1-a... Cluster is being health-checked (master is healthy)...done.
Created [https://container.googleapis.com/v1/projects/pelagic-script-244221/zones/us-west1-a/clusters/enkiserver].
To inspect the contents of your cluster, go to: https://console.cloud.google.com/kubernetes/workload_/gcloud/us-west1-a/enkiserver?project=pelagic-script-244221
kubeconfig entry generated for enkiserver.
NAME        LOCATION    MASTER_VERSION  MASTER_IP      MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
enkiserver  us-west1-a  1.13.7-gke.8    35.230.38.142  n1-standard-2  1.13.7-gke.8  2          RUNNING
```
Test the cluster
```
kubectl get node
```
```
Output:
NAME                                        STATUS   ROLES    AGE     VERSION
gke-enkiserver-default-pool-f46e18a1-k2c5   Ready    <none>   2m24s   v1.13.7-gke.8
gke-enkiserver-default-pool-f46e18a1-l32j   Ready    <none>   2m25s   v1.13.7-gke.8
```
Give your account permissions to perform all administrative actions needed.

```
kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole=cluster-admin \
  --user=ghiorso@gmail.com
```
```
Output:
clusterrolebinding.rbac.authorization.k8s.io/cluster-admin-binding created
```
Create a node pool for users
```
gcloud beta container node-pools create user-pool \
  --machine-type n1-standard-2 \
  --num-nodes 0 \
  --enable-autoscaling \
  --min-nodes 0 \
  --max-nodes 3 \
  --node-labels hub.jupyter.org/node-purpose=user \
  --node-taints hub.jupyter.org_dedicated=user:NoSchedule \
  --zone us-west1-a \
  --cluster enkiserver
```
```
Output:
Creating node pool user-pool...done.
Created [https://container.googleapis.com/v1beta1/projects/pelagic-script-244221/zones/us-west1-a/clusters/enkiserver/nodePools/user-pool].
NAME       MACHINE_TYPE   DISK_SIZE_GB  NODE_VERSION
user-pool  n1-standard-2  100           1.13.7-gke.8
```
#### Options: Expanding and contracting the size of your cluster
```
gcloud container clusters resize \
    <YOUR-CLUSTER-NAME> \
    --size <NEW-SIZE> \
    --zone <YOUR-CLUSTER-ZONE>
```
## Setup Helm
Helm is already installed in the *Google Cloud Shell*. Otherwise install it from:
```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
```
- Set up a ServiceAccount for use by tiller
```
kubectl --namespace kube-system create serviceaccount tiller
```
- Give the ServiceAccount full permissions to manage the cluster.

```
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
```
- Initialize helm and tiller
```
helm init --service-account tiller --wait
```
```
Output:
Creating /home/ghiorso/.helm
Creating /home/ghiorso/.helm/repository
Creating /home/ghiorso/.helm/repository/cache
Creating /home/ghiorso/.helm/repository/local
Creating /home/ghiorso/.helm/plugins
Creating /home/ghiorso/.helm/starters
Creating /home/ghiorso/.helm/cache/archive
Creating /home/ghiorso/.helm/repository/repositories.yaml
Adding stable repo with URL: https://kubernetes-charts.storage.googleapis.com
Adding local repo with URL: http://127.0.0.1:8879/charts
$HELM_HOME has been configured at /home/ghiorso/.helm.
Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.
Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
```
- Ensure that tiller is secure from access inside the cluster
```
kubectl patch deployment tiller-deploy --namespace=kube-system --type=json --patch='[{"op": "add", "path": "/spec/template/spec/containers/0/command", "value": ["/tiller", "--listen=localhost:44134"]}]'
```
- Check that Helm is functioning 
```
helm version
```
```
Output:
Client: &version.Version{SemVer:"v2.14.1", GitCommit:"5270352a09c7e8b6e8c9593002a73535276507c0", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.14.1", GitCommit:"5270352a09c7e8b6e8c9593002a73535276507c0", GitTreeState:"clean"}
```
## Set up JupyterHub
- Generate a random hex string representing 32 bytes to use as a security token. Run this command in a terminal and copy the output

```
openssl rand -hex 32
```
Output

```
782f65ff317054c3ca9e7942294b57fa79f61d99de85c63186acb4e83c2d03a1
```
### Create a minimal config.yaml file

```
nano config.yaml
```
Enter into this file
```
proxy:
  secretToken: "<RANDOM_HEX>"
```
#### Add options to config.yaml: Use Jupyter Lab by default

```
singleuser:
  defaultUrl: "/lab"

hub:
  extraConfig:
    jupyterlab: |
      c.Spawner.cmd = ['jupyter-labhub']
```
#### Add options to config.yaml: Use the scipy-notebook docker image
Get the latest image tag at:  
https://hub.docker.com/r/jupyter/datascience-notebook/tags/  
Inspect the Dockerfile at:  
https://github.com/jupyter/docker-stacks/tree/master/datascience-notebook/Dockerfile
    
```
singleuser:
  image:
    name: jupyter/scipy-notebook
    tag: d4cbf2f80a2a
```
#### Add options to config.yaml: Customize an existing Docker image
Example Dockerfile building on top of the minimal-notebook image. This file can be built to a docker image, and pushed to a image registry, and finally configured in config.yaml to be used by the Helm chart.
```
FROM jupyter/minimal-notebook:177037d09156
# Get the latest image tag at:
# https://hub.docker.com/r/jupyter/minimal-notebook/tags/
# Inspect the Dockerfile at:
# https://github.com/jupyter/docker-stacks/tree/master/minimal-notebook/Dockerfile

# install additional package...
RUN pip install --no-cache-dir astropy
```
#### Add options to config.yaml: Set environment variables
```
singleuser:
  extraEnv:
    EDITOR: "vim"
```
#### Add options to config.yaml: Using *nbgitpuller* to synchronize a folder
*nbgitpuller* must be installed in a custom docker image.  Add https://github.com/jupyterhub/nbgitpuller.git
```
singleuser:
  lifecycleHooks:
    postStart:
      exec:
        command: ["gitpuller", "https://github.com/data-8/materials-fa17", "master", "materials-fa"]
```
#### Add options to config.yaml: Using multiple profiles to let users select their environment
Here’s an example with four profiles that lets users select the environment they wish to use.
```
singleuser:
  # Defines the default image
  image:
    name: jupyter/minimal-notebook
    tag: 2343e33dec46
  profileList:
    - display_name: "Minimal environment"
      description: "To avoid too much bells and whistles: Python."
      default: true
    - display_name: "Datascience environment"
      description: "If you want the additional bells and whistles: Python, R, and Julia."
      kubespawner_override:
        image: jupyter/datascience-notebook:2343e33dec46
    - display_name: "Spark environment"
      description: "The Jupyter Stacks spark image!"
      kubespawner_override:
        image: jupyter/all-spark-notebook:2343e33dec46
    - display_name: "Learning Data Science"
      description: "Datascience Environment with Sample Notebooks"
      kubespawner_override:
        image: jupyter/datascience-notebook:2343e33dec46
        lifecycle_hooks:
          postStart:
            exec:
              command:
                - "sh"
                - "-c"
                - >
                  gitpuller https://github.com/data-8/materials-fa17 master materials-fa;
```
Note: You can also control the HTML used for the profile selection page by using the Kubespawner profile_form_template configuration. See the Kubespawner configuration reference for more information (https://jupyterhub-kubespawner.readthedocs.io/en/latest/spawner.html).
#### Add options to config.yaml: Set user memory and CPU guarantees / limits
```
singleuser:
  memory:
    limit: 1G
    guarantee: 1G
    
singleuser:
  cpu:
    limit: .5
    guarantee: .5
```
#### Add options to config.yaml: Additional storage volumes
If you already have a PersistentVolume and PersistentVolumeClaim created outside of JupyterHub you can mount them inside the user pods. For example, if you have a shared PersistentVolumeClaim called jupyterhub-shared-volume you could mount it as /home/shared in all user pods:
```
singleuser:
  storage:
    extraVolumes:
      - name: jupyterhub-shared
        persistentVolumeClaim:
          claimName: jupyterhub-shared-volume
    extraVolumeMounts:
      - name: jupyterhub-shared
        mountPath: /home/shared
```
#### Add options to config.yaml: Admin Users
```
auth:
  admin:
    users:
      - adminuser1
      - adminuser2
```
#### Add options to config.yaml: Adding a Whitelist
You can specify this list of usernames
```
auth:
  whitelist:
    users:
      - user1
      - user2
```
For example, here’s the configuration to use a white list along with the Dummy Authenticator. By default, the Dummy Authenticator will accept any username if they provide the right password. But combining it with a whitelist, users must input both an accepted username and password.
```
auth:
  type: dummy
  dummy:
    password: 'mypassword'
  whitelist:
    users:
      - user1
      - user2
```
#### Add options to config.yaml: Using GitLab for authentication
Create a GitLab OAuth application (https://docs.gitlab.com/ce/integration/oauth_provider.html).
Modify the following GitHub example:
```
auth:
  type: github
  github:
    clientId: "y0urg1thubc1ient1d"
    clientSecret: "an0ther1ongs3cretstr1ng"
    callbackUrl: "http://<your_jupyterhub_host>/hub/oauth_callback"
```
The configuration above will allow any GitHub user to access your JupyterHub. You can restrict access to a white-list of GitHub users by adding the following to your configuration.
```
auth:
  type: github
  admin:
    access: true
    users:
      - user1
      # ...
  whitelist:
    users:
      - user2
      # ...
```
Giving access to organizations on GitHub:
```
auth:
  type: github
  github:
    # ...
    orgWhitelist:
      - "SomeOrgName"
  scopes:
    - "read:user"
```
#### Add options to config.yaml: Optimizing for autoscaling
```
scheduling:
  userScheduler:
    enabled: true
  podPriority:
    enabled: true
  userPlaceholder:
    enabled: true
    replicas: 4
  userPods:
    nodeAffinity:
      matchNodePurpose: require

cull:
  enabled: true
  timeout: 3600
  every: 300

# The resources requested is very important to consider in
# relation to your machine type. If you have a n1-highmem-4 node
# on Google Cloud for example you get 4 cores and 26 GB of
# memory. With the configuration below you would  be able to have
# at most about 50 users per node. This can be reasonable, but it
# may not be, it will depend on your users. Are they mostly
# writing and reading or are they mostly executing code?
singleuser:
  cpu:
    limit: 4
    guarantee: 0.05
  memory:
    limit: 4G
    guarantee: 512M
```
#### Add options to config.yaml: Add security (https)
- Create an A record from the domain you want to use, pointing to the EXTERNAL-IP of the proxy-public service
- Wait for the change to propagate
- Set up automatic HTTPS
```
proxy:
  https:
    hosts:
      - <your-domain-name>
    letsencrypt:
      contactEmail: <your-email-address>
```
### Initialize Helm with the chart repository
- Make Helm aware of the JupyterHub Helm chart repository so you can install the JupyterHub chart from it without having to use a long URL name

```
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update
```
```
Output:
Hang tight while we grab the latest from your chart repositories...
...Skip local chart repository
...Successfully got an update from the "jupyterhub" chart repository
...Successfully got an update from the "stable" chart repository
Update Complete.
```
- now execute

```
# Suggested values: advanced users of Kubernetes and Helm should feel
# free to use different values.
RELEASE=jhub
NAMESPACE=jhub

helm upgrade --install $RELEASE jupyterhub/jupyterhub \
  --namespace $NAMESPACE  \
  --version=0.8.2 \
  --values config.yaml
```
```
Output:
NAME:   jhub
LAST DEPLOYED: Mon Jul  1 12:54:37 2019
NAMESPACE: jhub
STATUS: DEPLOYED
RESOURCES:
==> v1/ConfigMap
NAME        DATA  AGE
hub-config  1     1s
==> v1/Deployment
NAME   READY  UP-TO-DATE  AVAILABLE  AGE
hub    0/1    1           0          1s
proxy  0/1    1           0          1s
==> v1/PersistentVolumeClaim
NAME        STATUS   VOLUME    CAPACITY  ACCESS MODES  STORAGECLASS  AGE
hub-db-dir  Pending  standard  1s
==> v1/Pod(related)
NAME                    READY  STATUS             RESTARTS  AGE
hub-547476b74f-jh2pd    0/1    Pending            0         1s
proxy-6c474b6f78-r9pv9  0/1    ContainerCreating  0         1s
==> v1/Role
NAME  AGE
hub 1s

==> v1/RoleBinding
NAME  AGE
hub   1s
==> v1/Secret
NAME        TYPE    DATA  AGE
hub-secret  Opaque  2     1s
==> v1/Service
NAME          TYPE          CLUSTER-IP   EXTERNAL-IP  PORT(S)                     AGE
hub           ClusterIP     10.97.5.198  <none>       8081/TCP                    1s
proxy-api     ClusterIP     10.97.8.82   <none>       8001/TCP                    1s
proxy-public  LoadBalancer  10.97.10.31  <pending>    80:30081/TCP,443:31687/TCP  1s
==> v1/ServiceAccount
NAME  SECRETS  AGE
hub   1        1s
==> v1/StatefulSet
NAME              READY  AGE
user-placeholder  0/0    1s
==> v1beta1/PodDisruptionBudget
NAME              MIN AVAILABLE  MAX UNAVAILABLE  ALLOWED DISRUPTIONS  AGE
hub               1              N/A              0                    1s
proxy             1              N/A              0                    1s
user-placeholder  0              N/A              0                    1s
user-scheduler    1              N/A              0                    1s
NOTES:
Thank you for installing JupyterHub!
Your release is named jhub and installed into the namespace jhub.
You can find if the hub and proxy is ready by doing:
 kubectl --namespace=jhub get pod
and watching for both those pods to be in status 'Ready'.
You can find the public IP of the JupyterHub by doing:
 kubectl --namespace=jhub get svc proxy-public
It might take a few minutes for it to appear!
Note that this is still an alpha release! If you have questions, feel free to
  1. Read the guide at https://z2jh.jupyter.org
  2. Chat with us at https://gitter.im/jupyterhub/jupyterhub
  3. File issues at https://github.com/jupyterhub/zero-to-jupyterhub-k8s/issues
```
- you can see the pods being created by entering in a different terminal

```
kubectl get pod --namespace jhub
```
```
Output:
hub-547476b74f-jh2pd     1/1     Running   0          3m49s
proxy-6c474b6f78-r9pv9   1/1     Running   0          3m49s
```
- we recommend that you enable autocompletion for kubectl and set a default value for the --namespace flag

```
kubectl config set-context $(kubectl config current-context)
```
- Find the IP we can use to access the JupyterHub

```
kubectl get service --namespace jhub
```
```
Output:
NAME           TYPE           CLUSTER-IP    EXTERNAL-IP    PORT(S)                      AGE
hub            ClusterIP      10.97.5.198   <none>         8081/TCP                     8m32s
proxy-api      ClusterIP      10.97.8.82    <none>         8001/TCP                     8m32s
proxy-public   LoadBalancer   10.97.10.31   34.83.131.75   80:30081/TCP,443:31687/TCP   8m32s
```
## Tearing down the cluster
- First, delete the Helm release (use the release name from the helm update statement listed above)

```
helm delete <YOUR-HELM-RELEASE-NAME> --purge
```
- Next, delete the Kubernetes namespace the hub was installed in

```
kubectl delete namespace <YOUR-NAMESPACE>
```
- list all the clusters you have

```
gcloud container clusters list
```
- then delete the one you want

```
gcloud container clusters delete <CLUSTER-NAME> --zone=<CLUSTER-ZONE>
```
- Double check to make sure all the resources are now deleted.  From the web console:

```
At a minimum, check the following under the Hamburger (left top corner) menu:

Compute -> Compute Engine -> Disks
Compute -> Kubernetes Engine -> Clusters
Tools -> Container Registry -> Images
Networking -> Network Services -> Load Balancing
```
## Customizing your Deployment
- Make a change to your config.yaml
- Run a helm upgrade (helm list should display <YOUR_RELEASE_NAME> if you forgot it)

```
RELEASE=jhub

helm upgrade $RELEASE jupyterhub/jupyterhub \
  --version=0.8.2 \
  --values config.yaml
```
- Verify that the hub and proxy pods entered the Running state after the upgrade completed

```
kubectl get pod --namespace jhub
```
### Choose and use an existing Docker image

```
singleuser:
  image:
    # Get the latest image tag at:
    # https://hub.docker.com/r/jupyter/datascience-notebook/tags/
    # Inspect the Dockerfile at:
    # https://github.com/jupyter/docker-stacks/tree/master/datascience-notebook/Dockerfile
    name: jupyter/datascience-notebook
    tag: 177037d09156
```

# Customize a docker image

## Install docker Desktop
- https://hub.docker.com/
- (login at msghiorso to existing docker hub account)

### Tutorial (in terminal):
This repository contains everything you need to create your first container.
```
git clone https://github.com/docker/doodle.git
```
A Docker image is a private filesystem, just for your container. It provides all the files and code your container will need. Running the docker build command creates a Docker image using the Dockerfile. This built image is in your machine's local Docker image registry.

```
cd doodle/cheers2019
docker build -t msghiorso/cheers2019 .
```
```
Output:
Sending build context to Docker daemon   12.8kB
Step 1/9 : FROM golang:1.11-alpine AS builder
1.11-alpine: Pulling from library/golang
921b31ab772b: Pull complete 
2d3896533852: Pull complete 
28c34ce20860: Pull complete 
4396821e3d70: Pull complete 
3d496fc57ab1: Pull complete 
Digest: sha256:c64c2946ed2230af37101daca52c1354a613451a09b9098164791227dabeb5db
Status: Downloaded newer image for golang:1.11-alpine
 ---> 538699c669e2
Step 2/9 : RUN apk add --no-cache git
 ---> Running in 238c30bb50a8
fetch http://dl-cdn.alpinelinux.org/alpine/v3.10/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.10/community/x86_64/APKINDEX.tar.gz
(1/5) Installing nghttp2-libs (1.38.0-r0)
(2/5) Installing libcurl (7.65.1-r0)
(3/5) Installing expat (2.2.7-r0)
(4/5) Installing pcre2 (10.33-r0)
(5/5) Installing git (2.22.0-r0)
Executing busybox-1.30.1-r2.trigger
OK: 21 MiB in 20 packages
Removing intermediate container 238c30bb50a8
 ---> 307c6b3caca0
Step 3/9 : RUN go get github.com/pdevine/go-asciisprite
 ---> Running in 48fe1ceca72b
Removing intermediate container 48fe1ceca72b
 ---> 4fef6146906c
Step 4/9 : WORKDIR /project
 ---> Running in b645b41ec084
Removing intermediate container b645b41ec084
 ---> f603263c73a5
Step 5/9 : COPY cheers.go .
 ---> 9ba2a2b11bf2
Step 6/9 : RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o cheers cheers.go
 ---> Running in 6e05ece1a3fb
Removing intermediate container 6e05ece1a3fb
 ---> 55e338e2875a
Step 7/9 : FROM scratch
 ---> 
Step 8/9 : COPY --from=builder /project/cheers /cheers
 ---> e0511e38398c
Step 9/9 : ENTRYPOINT ["/cheers"]
 ---> Running in c7cf94281a15
Removing intermediate container c7cf94281a15
 ---> 754f653e50bc
Successfully built 754f653e50bc
Successfully tagged msghiorso/cheers2019:latest
```
Running a container launches your software with private resources, securely isolated from the rest of your machine.
```
docker run -it --rm msghiorso/cheers2019
```
[cute output in terminal window]  
Once you're ready to share your container with the world, push the image that describes it to Docker Hub.
docker login
docker push msghiorso/cheers2019

## Setup GitLab container registry to hold docker images

- Login to the GitLab container registry
```
docker login registry.gitlab.com
```
- You can also use a deploy token for read-only access to the registry images.
1. Log in to your GitLab account.
2. Go to the project you want to create Deploy Tokens for.
3. Go to Settings > Repository.
4. Click on "Expand" on Deploy Tokens section.
5. Choose a name and optionally an expiry date for the token.
6. Choose the desired scopes.
7. Click on Create deploy token.
8. Save the deploy token somewhere safe. Once you leave or refresh
the page, you won't be able to access it again.
```
docker login registry.example.com -u <username> -p <deploy_token>
```
- Once you log in, you’re free to create and upload a container image using the common build and push commands
```
docker build -t registry.gitlab.com/enki-portal/thermoengine .
docker push registry.gitlab.com/enki-portal/thermoengine
```
Use different image names
```
registry.gitlab.com/enki-portal/thermoengine:tag
registry.gitlab.com/enki-portal/thermoengine/optional-image-name:tag
registry.gitlab.com/enki-portal/thermoengine/optional-name/optional-image-name:tag
```

### Customize an existing Docker image
Dockerfile building on top of the scipy-notebook image. The docker image is built and pushed to the GitLab image registry. It is finally configured in config.yaml to be used by the Helm chart.  

Dockerfile (at root of ThermoEngine repository)
```
# Copyright (c) ENKI Development Team.
# Distributed under the terms of the Affero General Public License.
ARG BASE_CONTAINER=jupyter/scipy-notebook
FROM $BASE_CONTAINER

LABEL maintainer="Mark Ghiorso <ghiorso@ofm-research.org>"

USER root

# compilers required for ENKI
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    gfortran \
    gcc \
    cmake \
    clang && \
    rm -rf /var/lib/apt/lists/*

USER $NB_UID

# R packages including IRKernel which gets installed globally.
RUN conda install --quiet --yes \
    'snakeviz=2.0*' && \
    conda clean --all -f -y 

# install additional package...
RUN pip install --no-cache-dir nbgitpuller
```
To build a new image in the root of thw repository directory directory:
```
docker login registry.gitlab.com
```
using GitLab credentials.  Then,
```
docker build -t registry.gitlab.com/enki-portal/thermoengine/enki-scipy-notebook .
```
```
Output:
Sending build context to Docker daemon  216.9MB
Step 1/8 : ARG BASE_CONTAINER=jupyter/scipy-notebook
Step 2/8 : FROM $BASE_CONTAINER
latest: Pulling from jupyter/scipy-notebook
a48c500ed24e: Pull complete 
1e1de00ff7e1: Pull complete 
0330ca45a200: Pull complete 
471db38bcfbf: Pull complete 
0b4aba487617: Pull complete 
1bac85b3a63e: Pull complete 
245be47b44f6: Pull complete 
ef168d10cf08: Pull complete 
3f40baab49e8: Pull complete 
1074310668a8: Pull complete 
acab6d938518: Pull complete 
d3c413e667b9: Pull complete 
63b84d46215a: Pull complete 
e2aa43484a2e: Pull complete 
e45a3ec35504: Pull complete 
b91bbc043eab: Pull complete 
8842220992fc: Pull complete 
fc8f34d51deb: Pull complete 
5f6edb450186: Pull complete 
44257488fae5: Pull complete 
540df7774880: Pull complete 
178f3a1a18b4: Pull complete 
03528a45986d: Pull complete 
5c52a47b5569: Pull complete 
Digest: sha256:a2518b3a15c207dbe4997ac94c91966256ac94792b09844fbdfe53015927136f
Status: Downloaded newer image for jupyter/scipy-notebook:latest
 ---> 422d367c9f0d
Step 3/8 : LABEL maintainer="Mark Ghiorso <ghiorso@ofm-research.org>"
 ---> Running in 58110e2f9fee
Removing intermediate container 58110e2f9fee
 ---> 5881c6851953
Step 4/8 : USER root
 ---> Running in 9471b34b9152
Removing intermediate container 9471b34b9152
 ---> 705c41d7f7e1
Step 5/8 : RUN apt-get update &&     apt-get install -y --no-install-recommends     gfortran     gcc     cmake     clang &&     rm -rf /var/lib/apt/lists/*
 ---> Running in c34696814fe6
Get:1 http://archive.ubuntu.com/ubuntu bionic InRelease [242 kB]
Get:2 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB]
Get:3 http://archive.ubuntu.com/ubuntu bionic-updates InRelease [88.7 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic-backports InRelease [74.6 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic/universe Sources [11.5 MB]
Get:6 http://security.ubuntu.com/ubuntu bionic-security/universe Sources [188 kB]
Get:7 http://security.ubuntu.com/ubuntu bionic-security/multiverse amd64 Packages [4,169 B]
Get:8 http://security.ubuntu.com/ubuntu bionic-security/main amd64 Packages [576 kB]
Get:9 http://archive.ubuntu.com/ubuntu bionic/universe amd64 Packages [11.3 MB]
Get:10 http://security.ubuntu.com/ubuntu bionic-security/universe amd64 Packages [721 kB]
Get:11 http://security.ubuntu.com/ubuntu bionic-security/restricted amd64 Packages [5,436 B]
Get:12 http://archive.ubuntu.com/ubuntu bionic/restricted amd64 Packages [13.5 kB]
Get:13 http://archive.ubuntu.com/ubuntu bionic/multiverse amd64 Packages [186 kB]
Get:14 http://archive.ubuntu.com/ubuntu bionic/main amd64 Packages [1,344 kB]
Get:15 http://archive.ubuntu.com/ubuntu bionic-updates/universe Sources [328 kB]
Get:16 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 Packages [1,238 kB]
Get:17 http://archive.ubuntu.com/ubuntu bionic-updates/restricted amd64 Packages [10.8 kB]
Get:18 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 Packages [881 kB]
Get:19 http://archive.ubuntu.com/ubuntu bionic-updates/multiverse amd64 Packages [7,239 B]
Get:20 http://archive.ubuntu.com/ubuntu bionic-backports/universe amd64 Packages [3,927 B]
Get:21 http://archive.ubuntu.com/ubuntu bionic-backports/main amd64 Packages [2,496 B]
Fetched 28.9 MB in 3s (8,982 kB/s)
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following additional packages will be installed:
  clang-6.0 cmake-data cpp cpp-7 g++-7 gcc-7 gcc-7-base gcc-8-base gfortran-7
  lib32gcc1 lib32stdc++6 libarchive13 libasan4 libatomic1 libc6-i386 libcc1-0
  libcilkrts5 libclang-common-6.0-dev libclang1-6.0 libcurl4 libgcc-7-dev
  libgcc1 libgfortran-7-dev libgfortran4 libgomp1 libitm1 libjsoncpp1
  libllvm6.0 liblsan0 liblzo2-2 libmpx2 libobjc-7-dev libobjc4 libquadmath0
  librhash0 libstdc++-7-dev libstdc++6 libtsan0 libubsan0 libuv1
Suggested packages:
  gnustep gnustep-devel clang-6.0-doc cmake-doc ninja-build cpp-doc
  gcc-7-locales g++-7-multilib gcc-7-doc libstdc++6-7-dbg gcc-multilib
  manpages-dev autoconf automake libtool flex bison gdb gcc-doc gcc-7-multilib
  libgcc1-dbg libgomp1-dbg libitm1-dbg libatomic1-dbg libasan4-dbg
  liblsan0-dbg libtsan0-dbg libubsan0-dbg libcilkrts5-dbg libmpx2-dbg
  libquadmath0-dbg gfortran-multilib gfortran-doc gfortran-7-multilib
  gfortran-7-doc libgfortran4-dbg libcoarrays-dev lrzip libstdc++-7-doc
Recommended packages:
  llvm-6.0-dev libomp-dev
The following NEW packages will be installed:
  clang clang-6.0 cmake cmake-data gfortran gfortran-7 lib32gcc1 lib32stdc++6
  libarchive13 libc6-i386 libclang-common-6.0-dev libclang1-6.0 libcurl4
  libgfortran-7-dev libgfortran4 libjsoncpp1 libllvm6.0 liblzo2-2
  libobjc-7-dev libobjc4 librhash0 libuv1
The following packages will be upgraded:
  cpp cpp-7 g++-7 gcc gcc-7 gcc-7-base gcc-8-base libasan4 libatomic1 libcc1-0
  libcilkrts5 libgcc-7-dev libgcc1 libgomp1 libitm1 liblsan0 libmpx2
  libquadmath0 libstdc++-7-dev libstdc++6 libtsan0 libubsan0
22 upgraded, 22 newly installed, 0 to remove and 79 not upgraded.
Need to get 78.1 MB of archives.
After this operation, 243 MB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu bionic/main amd64 liblzo2-2 amd64 2.08-1.2 [48.7 kB]
Get:2 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libquadmath0 amd64 8.3.0-6ubuntu1~18.04.1 [133 kB]
Get:3 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libitm1 amd64 8.3.0-6ubuntu1~18.04.1 [28.0 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 gcc-8-base amd64 8.3.0-6ubuntu1~18.04.1 [18.7 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libstdc++6 amd64 8.3.0-6ubuntu1~18.04.1 [400 kB]
Get:6 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libmpx2 amd64 8.3.0-6ubuntu1~18.04.1 [11.6 kB]
Get:7 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 liblsan0 amd64 8.3.0-6ubuntu1~18.04.1 [133 kB]
Get:8 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libtsan0 amd64 8.3.0-6ubuntu1~18.04.1 [288 kB]
Get:9 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libcc1-0 amd64 8.3.0-6ubuntu1~18.04.1 [47.4 kB]
Get:10 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libatomic1 amd64 8.3.0-6ubuntu1~18.04.1 [9,184 B]
Get:11 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libgomp1 amd64 8.3.0-6ubuntu1~18.04.1 [76.4 kB]
Get:12 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libgcc1 amd64 1:8.3.0-6ubuntu1~18.04.1 [40.7 kB]
Get:13 http://archive.ubuntu.com/ubuntu bionic/main amd64 libjsoncpp1 amd64 1.7.4-3 [73.6 kB]
Get:14 http://archive.ubuntu.com/ubuntu bionic/main amd64 libllvm6.0 amd64 1:6.0-1ubuntu2 [14.5 MB]
Get:15 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libasan4 amd64 7.4.0-1ubuntu1~18.04.1 [359 kB]
Get:16 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libubsan0 amd64 7.4.0-1ubuntu1~18.04.1 [126 kB]
Get:17 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libcilkrts5 amd64 7.4.0-1ubuntu1~18.04.1 [42.5 kB]
Get:18 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 g++-7 amd64 7.4.0-1ubuntu1~18.04.1 [7,574 kB]
Get:19 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 gcc-7 amd64 7.4.0-1ubuntu1~18.04.1 [7,463 kB]
Get:20 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libstdc++-7-dev amd64 7.4.0-1ubuntu1~18.04.1 [1,468 kB]
Get:21 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libgcc-7-dev amd64 7.4.0-1ubuntu1~18.04.1 [2,381 kB]
Get:22 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 cpp-7 amd64 7.4.0-1ubuntu1~18.04.1 [6,742 kB]
Get:23 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 gcc-7-base amd64 7.4.0-1ubuntu1~18.04.1 [18.9 kB]
Get:24 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libobjc4 amd64 8.3.0-6ubuntu1~18.04.1 [52.2 kB]
Get:25 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libobjc-7-dev amd64 7.4.0-1ubuntu1~18.04.1 [206 kB]
Get:26 http://archive.ubuntu.com/ubuntu bionic/main amd64 libc6-i386 amd64 2.27-3ubuntu1 [2,651 kB]
Get:27 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 lib32gcc1 amd64 1:8.3.0-6ubuntu1~18.04.1 [47.9 kB]
Get:28 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 lib32stdc++6 amd64 8.3.0-6ubuntu1~18.04.1 [416 kB]
Get:29 http://archive.ubuntu.com/ubuntu bionic/universe amd64 libclang-common-6.0-dev amd64 1:6.0-1ubuntu2 [3,078 kB]
Get:30 http://archive.ubuntu.com/ubuntu bionic/main amd64 libclang1-6.0 amd64 1:6.0-1ubuntu2 [7,067 kB]
Get:31 http://archive.ubuntu.com/ubuntu bionic/universe amd64 clang-6.0 amd64 1:6.0-1ubuntu2 [9,292 kB]
Get:32 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 clang amd64 1:6.0-41~exp5~ubuntu1 [3,216 B]
Get:33 http://archive.ubuntu.com/ubuntu bionic/main amd64 cmake-data all 3.10.2-1ubuntu2 [1,331 kB]
Get:34 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libarchive13 amd64 3.2.2-3.1ubuntu0.3 [288 kB]
Get:35 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libcurl4 amd64 7.58.0-2ubuntu3.7 [214 kB]
Get:36 http://archive.ubuntu.com/ubuntu bionic/main amd64 librhash0 amd64 1.3.6-2 [78.1 kB]
Get:37 http://archive.ubuntu.com/ubuntu bionic/main amd64 libuv1 amd64 1.18.0-3 [64.4 kB]
Get:38 http://archive.ubuntu.com/ubuntu bionic/main amd64 cmake amd64 3.10.2-1ubuntu2 [3,138 kB]
Get:39 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 cpp amd64 4:7.4.0-1ubuntu2.3 [27.7 kB]
Get:40 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 gcc amd64 4:7.4.0-1ubuntu2.3 [5,184 B]
Get:41 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libgfortran4 amd64 7.4.0-1ubuntu1~18.04.1 [492 kB]
Get:42 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libgfortran-7-dev amd64 7.4.0-1ubuntu1~18.04.1 [530 kB]
Get:43 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 gfortran-7 amd64 7.4.0-1ubuntu1~18.04.1 [7,112 kB]
Get:44 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 gfortran amd64 4:7.4.0-1ubuntu2.3 [1,356 B]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 78.1 MB in 4s (19.9 MB/s)
Selecting previously unselected package liblzo2-2:amd64.
(Reading database ... 119112 files and directories currently installed.)
Preparing to unpack .../liblzo2-2_2.08-1.2_amd64.deb ...
Unpacking liblzo2-2:amd64 (2.08-1.2) ...
Preparing to unpack .../libquadmath0_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libquadmath0:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Preparing to unpack .../libitm1_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libitm1:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Preparing to unpack .../gcc-8-base_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking gcc-8-base:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Setting up gcc-8-base:amd64 (8.3.0-6ubuntu1~18.04.1) ...
(Reading database ... 119120 files and directories currently installed.)
Preparing to unpack .../libstdc++6_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libstdc++6:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Setting up libstdc++6:amd64 (8.3.0-6ubuntu1~18.04.1) ...
(Reading database ... 119120 files and directories currently installed.)
Preparing to unpack .../0-libmpx2_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libmpx2:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Preparing to unpack .../1-liblsan0_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking liblsan0:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Preparing to unpack .../2-libtsan0_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libtsan0:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Preparing to unpack .../3-libcc1-0_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libcc1-0:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Preparing to unpack .../4-libatomic1_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libatomic1:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Preparing to unpack .../5-libgomp1_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libgomp1:amd64 (8.3.0-6ubuntu1~18.04.1) over (8.3.0-6ubuntu1~18.04) ...
Preparing to unpack .../6-libgcc1_1%3a8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libgcc1:amd64 (1:8.3.0-6ubuntu1~18.04.1) over (1:8.3.0-6ubuntu1~18.04) ...
Setting up libgcc1:amd64 (1:8.3.0-6ubuntu1~18.04.1) ...
Selecting previously unselected package libjsoncpp1:amd64.
(Reading database ... 119120 files and directories currently installed.)
Preparing to unpack .../00-libjsoncpp1_1.7.4-3_amd64.deb ...
Unpacking libjsoncpp1:amd64 (1.7.4-3) ...
Selecting previously unselected package libllvm6.0:amd64.
Preparing to unpack .../01-libllvm6.0_1%3a6.0-1ubuntu2_amd64.deb ...
Unpacking libllvm6.0:amd64 (1:6.0-1ubuntu2) ...
Preparing to unpack .../02-libasan4_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking libasan4:amd64 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Preparing to unpack .../03-libubsan0_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking libubsan0:amd64 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Preparing to unpack .../04-libcilkrts5_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking libcilkrts5:amd64 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Preparing to unpack .../05-g++-7_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking g++-7 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Preparing to unpack .../06-gcc-7_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking gcc-7 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Preparing to unpack .../07-libstdc++-7-dev_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking libstdc++-7-dev:amd64 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Preparing to unpack .../08-libgcc-7-dev_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking libgcc-7-dev:amd64 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Preparing to unpack .../09-cpp-7_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking cpp-7 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Preparing to unpack .../10-gcc-7-base_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking gcc-7-base:amd64 (7.4.0-1ubuntu1~18.04.1) over (7.4.0-1ubuntu1~18.04) ...
Selecting previously unselected package libobjc4:amd64.
Preparing to unpack .../11-libobjc4_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking libobjc4:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Selecting previously unselected package libobjc-7-dev:amd64.
Preparing to unpack .../12-libobjc-7-dev_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking libobjc-7-dev:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Selecting previously unselected package libc6-i386.
Preparing to unpack .../13-libc6-i386_2.27-3ubuntu1_amd64.deb ...
Unpacking libc6-i386 (2.27-3ubuntu1) ...
Selecting previously unselected package lib32gcc1.
Preparing to unpack .../14-lib32gcc1_1%3a8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking lib32gcc1 (1:8.3.0-6ubuntu1~18.04.1) ...
Selecting previously unselected package lib32stdc++6.
Preparing to unpack .../15-lib32stdc++6_8.3.0-6ubuntu1~18.04.1_amd64.deb ...
Unpacking lib32stdc++6 (8.3.0-6ubuntu1~18.04.1) ...
Selecting previously unselected package libclang-common-6.0-dev.
Preparing to unpack .../16-libclang-common-6.0-dev_1%3a6.0-1ubuntu2_amd64.deb ...
Unpacking libclang-common-6.0-dev (1:6.0-1ubuntu2) ...
Selecting previously unselected package libclang1-6.0:amd64.
Preparing to unpack .../17-libclang1-6.0_1%3a6.0-1ubuntu2_amd64.deb ...
Unpacking libclang1-6.0:amd64 (1:6.0-1ubuntu2) ...
Selecting previously unselected package clang-6.0.
Preparing to unpack .../18-clang-6.0_1%3a6.0-1ubuntu2_amd64.deb ...
Unpacking clang-6.0 (1:6.0-1ubuntu2) ...
Selecting previously unselected package clang.
Preparing to unpack .../19-clang_1%3a6.0-41~exp5~ubuntu1_amd64.deb ...
Unpacking clang (1:6.0-41~exp5~ubuntu1) ...
Selecting previously unselected package cmake-data.
Preparing to unpack .../20-cmake-data_3.10.2-1ubuntu2_all.deb ...
Unpacking cmake-data (3.10.2-1ubuntu2) ...
Selecting previously unselected package libarchive13:amd64.
Preparing to unpack .../21-libarchive13_3.2.2-3.1ubuntu0.3_amd64.deb ...
Unpacking libarchive13:amd64 (3.2.2-3.1ubuntu0.3) ...
Selecting previously unselected package libcurl4:amd64.
Preparing to unpack .../22-libcurl4_7.58.0-2ubuntu3.7_amd64.deb ...
Unpacking libcurl4:amd64 (7.58.0-2ubuntu3.7) ...
Selecting previously unselected package librhash0:amd64.
Preparing to unpack .../23-librhash0_1.3.6-2_amd64.deb ...
Unpacking librhash0:amd64 (1.3.6-2) ...
Selecting previously unselected package libuv1:amd64.
Preparing to unpack .../24-libuv1_1.18.0-3_amd64.deb ...
Unpacking libuv1:amd64 (1.18.0-3) ...
Selecting previously unselected package cmake.
Preparing to unpack .../25-cmake_3.10.2-1ubuntu2_amd64.deb ...
Unpacking cmake (3.10.2-1ubuntu2) ...
Preparing to unpack .../26-cpp_4%3a7.4.0-1ubuntu2.3_amd64.deb ...
Unpacking cpp (4:7.4.0-1ubuntu2.3) over (4:7.4.0-1ubuntu2.2) ...
Preparing to unpack .../27-gcc_4%3a7.4.0-1ubuntu2.3_amd64.deb ...
Unpacking gcc (4:7.4.0-1ubuntu2.3) over (4:7.4.0-1ubuntu2.2) ...
Selecting previously unselected package libgfortran4:amd64.
Preparing to unpack .../28-libgfortran4_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking libgfortran4:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Selecting previously unselected package libgfortran-7-dev:amd64.
Preparing to unpack .../29-libgfortran-7-dev_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking libgfortran-7-dev:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Selecting previously unselected package gfortran-7.
Preparing to unpack .../30-gfortran-7_7.4.0-1ubuntu1~18.04.1_amd64.deb ...
Unpacking gfortran-7 (7.4.0-1ubuntu1~18.04.1) ...
Selecting previously unselected package gfortran.
Preparing to unpack .../31-gfortran_4%3a7.4.0-1ubuntu2.3_amd64.deb ...
Unpacking gfortran (4:7.4.0-1ubuntu2.3) ...
Setting up libquadmath0:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Setting up libgomp1:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Setting up libatomic1:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Setting up libcc1-0:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Setting up libobjc4:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Setting up libllvm6.0:amd64 (1:6.0-1ubuntu2) ...
Setting up libuv1:amd64 (1.18.0-3) ...
Setting up libtsan0:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Setting up libcurl4:amd64 (7.58.0-2ubuntu3.7) ...
Setting up libclang1-6.0:amd64 (1:6.0-1ubuntu2) ...
Setting up cmake-data (3.10.2-1ubuntu2) ...
Install cmake-data for emacs25
install/cmake-data: Byte-compiling for emacs25
Setting up libc6-i386 (2.27-3ubuntu1) ...
Setting up librhash0:amd64 (1.3.6-2) ...
Setting up liblsan0:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Setting up gcc-7-base:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up libmpx2:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Setting up libitm1:amd64 (8.3.0-6ubuntu1~18.04.1) ...
Setting up lib32gcc1 (1:8.3.0-6ubuntu1~18.04.1) ...
Setting up liblzo2-2:amd64 (2.08-1.2) ...
Setting up libjsoncpp1:amd64 (1.7.4-3) ...
Setting up libasan4:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up libgfortran4:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up libarchive13:amd64 (3.2.2-3.1ubuntu0.3) ...
Setting up libcilkrts5:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up libubsan0:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up libgcc-7-dev:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up cpp-7 (7.4.0-1ubuntu1~18.04.1) ...
Setting up libstdc++-7-dev:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up libobjc-7-dev:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up libgfortran-7-dev:amd64 (7.4.0-1ubuntu1~18.04.1) ...
Setting up lib32stdc++6 (8.3.0-6ubuntu1~18.04.1) ...
Setting up cmake (3.10.2-1ubuntu2) ...
Setting up cpp (4:7.4.0-1ubuntu2.3) ...
Setting up libclang-common-6.0-dev (1:6.0-1ubuntu2) ...
Setting up gcc-7 (7.4.0-1ubuntu1~18.04.1) ...
Setting up g++-7 (7.4.0-1ubuntu1~18.04.1) ...
Setting up gcc (4:7.4.0-1ubuntu2.3) ...
Setting up clang-6.0 (1:6.0-1ubuntu2) ...
Setting up gfortran-7 (7.4.0-1ubuntu1~18.04.1) ...
Setting up gfortran (4:7.4.0-1ubuntu2.3) ...
update-alternatives: using /usr/bin/gfortran to provide /usr/bin/f95 (f95) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/f95.1.gz because associated file /usr/share/man/man1/gfortran.1.gz (of link group f95) doesn't exist
update-alternatives: using /usr/bin/gfortran to provide /usr/bin/f77 (f77) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/f77.1.gz because associated file /usr/share/man/man1/gfortran.1.gz (of link group f77) doesn't exist
Setting up clang (1:6.0-41~exp5~ubuntu1) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Removing intermediate container c34696814fe6
 ---> 7e547153637d
Step 6/8 : USER $NB_UID
 ---> Running in 985b391c9a62
Removing intermediate container 985b391c9a62
 ---> f0ef30e90ac2
Step 7/8 : RUN conda install --quiet --yes     'snakeviz=2.0*' &&     conda clean --all -f -y
 ---> Running in e4d71fed5724
Collecting package metadata: ...working... done
Solving environment: ...working... 
The environment is inconsistent, please check the package plan carefully
The following packages are causing the inconsistency:

  - conda-forge/linux-64::ncurses==6.1=hf484d3e_1002
  - conda-forge/linux-64::yaml==0.1.7=h14c3975_1001
  - conda-forge/linux-64::six==1.12.0=py37_1000
  - conda-forge/linux-64::pyopenssl==19.0.0=py37_0
  - conda-forge/linux-64::pycosat==0.6.3=py37h14c3975_1001
  - conda-forge/linux-64::setuptools==41.0.1=py37_0
  - conda-forge/linux-64::chardet==3.0.4=py37_1003
  - conda-forge/linux-64::cffi==1.12.3=py37h8022711_0
  - conda-forge/linux-64::readline==7.0=hf8c457e_1001
  - conda-forge/linux-64::zlib==1.2.11=h14c3975_1004
  - conda-forge/linux-64::urllib3==1.24.2=py37_0
  - defaults/linux-64::libgcc-ng==8.2.0=hdf63c60_1
  - conda-forge/linux-64::sqlite==3.26.0=h67949de_1001
  - conda-forge/linux-64::libedit==3.1.20170329=hf8c457e_1001
  - conda-forge/linux-64::requests==2.21.0=py37_1000
  - conda-forge/linux-64::certifi==2019.3.9=py37_0
  - conda-forge/linux-64::ruamel_yaml==0.15.71=py37h14c3975_1000
  - conda-forge/linux-64::libffi==3.2.1=he1b5a44_1006
  - conda-forge/linux-64::xz==5.2.4=h14c3975_1001
  - conda-forge/linux-64::openssl==1.1.1b=h14c3975_1
  - conda-forge/linux-64::wheel==0.33.1=py37_0
  - conda-forge/linux-64::idna==2.8=py37_1000
  - conda-forge/linux-64::conda==4.6.14=py37_0
  - conda-forge/linux-64::asn1crypto==0.24.0=py37_1003
  - conda-forge/linux-64::tk==8.6.9=h84994c4_1001
  - conda-forge/linux-64::python==3.7.3=h5b0a415_0
  - conda-forge/linux-64::pysocks==1.6.8=py37_1002
  - conda-forge/linux-64::bzip2==1.0.6=h14c3975_1002
  - conda-forge/linux-64::pip==19.1=py37_0
  - conda-forge/linux-64::cryptography==2.6.1=py37h72c5cf5_0
  - conda-forge/linux-64::pycparser==2.19=py37_1
  - conda-forge/linux-64::pcre==8.41=hf484d3e_1003
  - conda-forge/linux-64::numpy==1.15.4=py37h8b7e671_1002
  - conda-forge/linux-64::xorg-libxau==1.0.9=h14c3975_0
  - conda-forge/linux-64::h5py==2.9.0=nompi_py37hf008753_1102
  - conda-forge/linux-64::statsmodels==0.9.0=py37h3010b51_1000
  - conda-forge/linux-64::libcblas==3.8.0=9_openblas
  - conda-forge/linux-64::protobuf==3.7.1=py37he1b5a44_0
  - conda-forge/linux-64::libiconv==1.15=h516909a_1005
  - conda-forge/linux-64::openblas==0.3.6=h6e990d7_2
  - conda-forge/linux-64::jpeg==9c=h14c3975_1001
  - conda-forge/noarch::partd==0.3.9=py_0
  - conda-forge/linux-64::matplotlib==3.0.3=py37_1
  - conda-forge/noarch::zict==0.1.4=py_0
  - conda-forge/noarch::mpmath==1.1.0=py_0
  - conda-forge/linux-64::pthread-stubs==0.4=h14c3975_1001
  - conda-forge/linux-64::cython==0.29.7=py37he1b5a44_0
  - conda-forge/noarch::vincent==0.4.4=py_1
  - conda-forge/linux-64::dbus==1.13.6=he372182_0
  - conda-forge/linux-64::llvmlite==0.27.1=py37hdbcaa40_0
  - conda-forge/linux-64::liblapack==3.8.0=9_openblas
  - conda-forge/linux-64::msgpack-python==0.6.1=py37h6bb024c_0
  - conda-forge/linux-64::heapdict==1.0.0=py37_1000
  - conda-forge/linux-64::libtiff==4.0.10=h648cc4a_1001
  - conda-forge/linux-64::pywavelets==1.0.3=py37hd352d35_1
  - conda-forge/linux-64::libxml2==2.9.9=h13577e0_0
  - conda-forge/linux-64::mpfr==4.0.2=ha14ba45_0
  - conda-forge/linux-64::hdf5==1.10.4=nompi_h3c11f04_1106
  - conda-forge/noarch::sortedcontainers==2.1.0=py_0
  - conda-forge/noarch::locket==0.2.0=py_2
  - conda-forge/linux-64::expat==2.2.5=hf484d3e_1002
  - conda-forge/linux-64::pyyaml==5.1=py37h14c3975_0
  - conda-forge/linux-64::glib==2.58.3=hf63aee3_1001
  - conda-forge/noarch::pytz==2019.1=py_0
  - conda-forge/linux-64::gmpy2==2.0.8=py37hb20f59a_1002
  - conda-forge/linux-64::libblas==3.8.0=9_openblas
  - conda-forge/linux-64::freetype==2.10.0=he983fc9_0
  - conda-forge/linux-64::pillow==6.0.0=py37he7afcd5_0
  - conda-forge/noarch::packaging==19.0=py_0
  - conda-forge/linux-64::distributed==1.28.0=py37_0
  - conda-forge/noarch::pyparsing==2.4.0=py_0
  - conda-forge/linux-64::mpc==1.1.0=hb20f59a_1006
  - conda-forge/linux-64::gst-plugins-base==1.14.4=hdf3bae2_1001
  - conda-forge/linux-64::bokeh==1.0.4=py37_1000
  - conda-forge/noarch::toolz==0.9.0=py_1
  - conda-forge/linux-64::scikit-learn==0.20.3=py37ha8026db_1
  - conda-forge/noarch::olefile==0.46=py_0
  - conda-forge/linux-64::matplotlib-base==3.0.3=py37h5f35d83_1
  - conda-forge/noarch::dask==1.1.5=py_0
  - conda-forge/linux-64::psutil==5.6.2=py37h516909a_0
  - conda-forge/noarch::xlrd==1.2.0=py_0
  - conda-forge/noarch::dask-core==1.1.5=py_0
  - conda-forge/linux-64::liblapacke==3.8.0=9_openblas
  - conda-forge/linux-64::fastcache==1.1.0=py37h516909a_0
  - conda-forge/linux-64::gstreamer==1.14.4=h66beb1c_1001
  - conda-forge/linux-64::kiwisolver==1.1.0=py37hc9558a2_0
  - conda-forge/linux-64::numexpr==2.6.9=py37h637b7d7_1000
  - conda-forge/linux-64::libprotobuf==3.7.1=h8b12597_0
  - conda-forge/noarch::cloudpickle==0.8.1=py_0
  - conda-forge/linux-64::soupsieve==1.9.1=py37_0
  - conda-forge/linux-64::gmp==6.1.2=hf484d3e_1000
  - conda-forge/linux-64::fontconfig==2.13.1=he4413a7_1000
  - conda-forge/linux-64::gettext==0.19.8.1=hc5be6a0_1002
  - conda-forge/noarch::patsy==0.5.1=py_0
  - conda-forge/linux-64::widgetsnbextension==3.4.2=py37_1000
  - conda-forge/linux-64::cytoolz==0.9.0.1=py37h14c3975_1001
  - conda-forge/linux-64::scikit-image==0.14.2=py37hf484d3e_1
  - conda-forge/linux-64::sip==4.19.8=py37hf484d3e_1000
  - conda-forge/linux-64::sympy==1.3=py37_1000
  - conda-forge/linux-64::libxcb==1.13=h14c3975_1002
  - conda-forge/linux-64::beautifulsoup4==4.7.1=py37_1001
  - conda-forge/linux-64::icu==58.2=hf484d3e_1000
  - conda-forge/linux-64::blas==2.9=openblas
  - conda-forge/linux-64::pandas==0.24.2=py37hf484d3e_0
  - conda-forge/noarch::ipywidgets==7.4.2=py_0
  - conda-forge/noarch::tblib==1.3.2=py_1
  - conda-forge/linux-64::xorg-libxdmcp==1.1.3=h516909a_0
  - conda-forge/linux-64::dill==0.2.9=py37_0
  - conda-forge/noarch::cycler==0.10.0=py_1
  - conda-forge/linux-64::imageio==2.5.0=py37_0
  - conda-forge/linux-64::libpng==1.6.37=hed695b0_0
  - conda-forge/linux-64::libuuid==2.32.1=h14c3975_1000
  - conda-forge/linux-64::scipy==1.2.1=py37h09a28d5_1
  - conda-forge/noarch::networkx==2.3=py_0
  - conda-forge/noarch::seaborn==0.9.0=py_1
  - conda-forge/linux-64::numba==0.42.1=py37hf484d3e_0
  - conda-forge/noarch::click==7.0=py_0
  - conda-forge/noarch::jupyterlab_server==0.2.0=py_0
  - conda-forge/linux-64::pycurl==7.43.0.2=py37h16ce93b_0
  - conda-forge/linux-64::notebook==5.7.8=py37_0
  - conda-forge/linux-64::sqlalchemy==1.3.3=py37h516909a_0
  - conda-forge/linux-64::libcurl==7.64.1=hda55be3_0
  - conda-forge/noarch::ipython_genutils==0.2.0=py_1
  - conda-forge/linux-64::zeromq==4.3.1=hf484d3e_1000
  - conda-forge/noarch::prompt_toolkit==2.0.9=py_0
  - conda-forge/noarch::webencodings==0.5.1=py_1
  - conda-forge/linux-64::libssh2==1.8.2=h22169c7_2
  - conda-forge/linux-64::terminado==0.8.2=py37_0
  - conda-forge/noarch::backcall==0.1.0=py_0
  - conda-forge/noarch::nbconvert==5.5.0=py_0
  - conda-forge/noarch::pandocfilters==1.4.2=py_1
  - conda-forge/noarch::attrs==19.1.0=py_0
  - conda-forge/linux-64::pyrsistent==0.15.2=py37h516909a_0
  - conda-forge/noarch::pygments==2.4.0=py_0
  - conda-forge/noarch::parso==0.4.0=py_0
  - conda-forge/noarch::nbformat==4.4.0=py_1
  - conda-forge/noarch::testpath==0.4.2=py_1001
  - conda-forge/noarch::pamela==1.0.0=py_0
  - conda-forge/noarch::decorator==4.4.0=py_0
  - conda-forge/noarch::oauthlib==3.0.1=py_0
  - conda-forge/noarch::wcwidth==0.1.7=py_1
  - conda-forge/linux-64::traitlets==4.3.2=py37_1000
  - conda-forge/linux-64::jupyterlab==0.35.5=py37_0
  - conda-forge/linux-64::entrypoints==0.3=py37_1000
  - conda-forge/noarch::jupyter_core==4.4.0=py_0
  - conda-forge/noarch::defusedxml==0.5.0=py_1
  - conda-forge/noarch::mako==1.0.7=py_1
  - conda-forge/linux-64::nodejs==11.14.0=he1b5a44_1
  - conda-forge/linux-64::jsonschema==3.0.1=py37_0
  - conda-forge/noarch::certipy==0.1.3=py_0
  - conda-forge/noarch::bleach==3.1.0=py_0
  - conda-forge/linux-64::tornado==6.0.2=py37h516909a_0
  - conda-forge/linux-64::jedi==0.13.3=py37_0
  - conda-forge/linux-64::mistune==0.8.4=py37h14c3975_1000
  - conda-forge/noarch::async_generator==1.10=py_0
  - conda-forge/linux-64::pexpect==4.7.0=py37_0
  - conda-forge/linux-64::krb5==1.16.3=h05b26f9_1001
  - conda-forge/noarch::jupyter_client==5.2.4=py_3
  - conda-forge/noarch::pyjwt==1.7.1=py_0
  - conda-forge/noarch::jinja2==2.10.1=py_0
  - conda-forge/linux-64::configurable-http-proxy==1.3.0=0
  - conda-forge/linux-64::libsodium==1.0.16=h14c3975_1001
  - conda-forge/noarch::python-editor==1.0.4=py_0
  - conda-forge/noarch::alembic==1.0.8=py_0
  - conda-forge/linux-64::pyzmq==18.0.1=py37hc4ba49a_1
  - conda-forge/noarch::send2trash==1.5.0=py_0
  - conda-forge/noarch::ptyprocess==0.6.0=py_1001
  - conda-forge/linux-64::ipykernel==5.1.0=py37h24bf2e0_1002
  - conda-forge/linux-64::ipython==7.5.0=py37h24bf2e0_0
  - conda-forge/linux-64::markupsafe==1.1.1=py37h14c3975_0
  - conda-forge/noarch::prometheus_client==0.6.0=py_0
  - conda-forge/noarch::python-dateutil==2.8.0=py_0
  - conda-forge/linux-64::jupyterhub==1.0.0=py37_0
  - conda-forge/noarch::blinker==1.4=py_1
  - conda-forge/linux-64::pickleshare==0.7.5=py37_1000
  - conda-forge/linux-64::tini==0.18.0=h14c3975_1001
done

## Package Plan ##

  environment location: /opt/conda

  added / updated specs:
    - snakeviz=2.0


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    _libgcc_mutex-0.1          |             main           3 KB  defaults
    ca-certificates-2019.6.16  |       hecc5488_0         145 KB  conda-forge
    certifi-2019.6.16          |           py37_0         148 KB  conda-forge
    libgcc-ng-9.1.0            |       hdf63c60_0         8.1 MB  defaults
    pip-19.1.1                 |           py37_0         1.8 MB  conda-forge
    python-3.7.3               |       h33d41f4_1        36.0 MB  conda-forge
    readline-8.0               |       hf8c457e_0         441 KB  conda-forge
    snakeviz-2.0.0             |             py_0         166 KB  conda-forge
    sqlite-3.28.0              |       hcee41ef_1         1.9 MB  conda-forge
    tk-8.6.9                   |    hed695b0_1002         3.2 MB  conda-forge
    tornado-6.0.3              |   py37h516909a_0         637 KB  conda-forge
    wheel-0.33.4               |           py37_0          34 KB  conda-forge
    ------------------------------------------------------------
                                           Total:        52.5 MB

The following NEW packages will be INSTALLED:

  _libgcc_mutex      pkgs/main/linux-64::_libgcc_mutex-0.1-main
  snakeviz           conda-forge/noarch::snakeviz-2.0.0-py_0

The following packages will be UPDATED:

  ca-certificates                       2019.3.9-hecc5488_0 --> 2019.6.16-hecc5488_0
  certifi                                   2019.3.9-py37_0 --> 2019.6.16-py37_0
  libgcc-ng                                8.2.0-hdf63c60_1 --> 9.1.0-hdf63c60_0
  pip                                           19.1-py37_0 --> 19.1.1-py37_0
  python                                   3.7.3-h5b0a415_0 --> 3.7.3-h33d41f4_1
  readline                                7.0-hf8c457e_1001 --> 8.0-hf8c457e_0
  sqlite                               3.26.0-h67949de_1001 --> 3.28.0-hcee41ef_1
  tk                                    8.6.9-h84994c4_1001 --> 8.6.9-hed695b0_1002
  tornado                              6.0.2-py37h516909a_0 --> 6.0.3-py37h516909a_0
  wheel                                       0.33.1-py37_0 --> 0.33.4-py37_0


Preparing transaction: ...working... done
Verifying transaction: ...working... done
Executing transaction: ...working... done
Remove all contents from the following package caches?
  - /opt/conda/pkgs
Removing intermediate container e4d71fed5724
 ---> 4fa15c0fdff1
Step 8/8 : RUN pip install --no-cache-dir nbgitpuller
 ---> Running in 8efcd491b74c
Collecting nbgitpuller
  Downloading https://files.pythonhosted.org/packages/db/07/0019b4a4d4b3a385503802b9a12889014dd6ae675fc88e0ce7127e345974/nbgitpuller-0.6.1-py2.py3-none-any.whl (289kB)
Requirement already satisfied: notebook>=5.5.0 in /opt/conda/lib/python3.7/site-packages (from nbgitpuller) (5.7.8)
Requirement already satisfied: tornado in /opt/conda/lib/python3.7/site-packages (from nbgitpuller) (6.0.3)
Requirement already satisfied: nbconvert in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (5.5.0)
Requirement already satisfied: Send2Trash in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (1.5.0)
Requirement already satisfied: terminado>=0.8.1 in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (0.8.2)
Requirement already satisfied: prometheus-client in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (0.6.0)
Requirement already satisfied: ipython-genutils in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (0.2.0)
Requirement already satisfied: ipykernel in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (5.1.0)
Requirement already satisfied: jupyter-client>=5.2.0 in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (5.2.4)
Requirement already satisfied: nbformat in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (4.4.0)
Requirement already satisfied: jupyter-core>=4.4.0 in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (4.4.0)
Requirement already satisfied: jinja2 in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (2.10.1)
Requirement already satisfied: pyzmq>=17 in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (18.0.1)
Requirement already satisfied: traitlets>=4.2.1 in /opt/conda/lib/python3.7/site-packages (from notebook>=5.5.0->nbgitpuller) (4.3.2)
Requirement already satisfied: mistune>=0.8.1 in /opt/conda/lib/python3.7/site-packages (from nbconvert->notebook>=5.5.0->nbgitpuller) (0.8.4)
Requirement already satisfied: entrypoints>=0.2.2 in /opt/conda/lib/python3.7/site-packages (from nbconvert->notebook>=5.5.0->nbgitpuller) (0.3)
Requirement already satisfied: defusedxml in /opt/conda/lib/python3.7/site-packages (from nbconvert->notebook>=5.5.0->nbgitpuller) (0.5.0)
Requirement already satisfied: testpath in /opt/conda/lib/python3.7/site-packages (from nbconvert->notebook>=5.5.0->nbgitpuller) (0.4.2)
Requirement already satisfied: pandocfilters>=1.4.1 in /opt/conda/lib/python3.7/site-packages (from nbconvert->notebook>=5.5.0->nbgitpuller) (1.4.2)
Requirement already satisfied: pygments in /opt/conda/lib/python3.7/site-packages (from nbconvert->notebook>=5.5.0->nbgitpuller) (2.4.0)
Requirement already satisfied: bleach in /opt/conda/lib/python3.7/site-packages (from nbconvert->notebook>=5.5.0->nbgitpuller) (3.1.0)
Requirement already satisfied: ipython>=5.0.0 in /opt/conda/lib/python3.7/site-packages (from ipykernel->notebook>=5.5.0->nbgitpuller) (7.5.0)
Requirement already satisfied: python-dateutil>=2.1 in /opt/conda/lib/python3.7/site-packages (from jupyter-client>=5.2.0->notebook>=5.5.0->nbgitpuller) (2.8.0)
Requirement already satisfied: jsonschema!=2.5.0,>=2.4 in /opt/conda/lib/python3.7/site-packages (from nbformat->notebook>=5.5.0->nbgitpuller) (3.0.1)
Requirement already satisfied: MarkupSafe>=0.23 in /opt/conda/lib/python3.7/site-packages (from jinja2->notebook>=5.5.0->nbgitpuller) (1.1.1)
Requirement already satisfied: six in /opt/conda/lib/python3.7/site-packages (from traitlets>=4.2.1->notebook>=5.5.0->nbgitpuller) (1.12.0)
Requirement already satisfied: decorator in /opt/conda/lib/python3.7/site-packages (from traitlets>=4.2.1->notebook>=5.5.0->nbgitpuller) (4.4.0)
Requirement already satisfied: webencodings in /opt/conda/lib/python3.7/site-packages (from bleach->nbconvert->notebook>=5.5.0->nbgitpuller) (0.5.1)
Requirement already satisfied: backcall in /opt/conda/lib/python3.7/site-packages (from ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (0.1.0)
Requirement already satisfied: jedi>=0.10 in /opt/conda/lib/python3.7/site-packages (from ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (0.13.3)
Requirement already satisfied: setuptools>=18.5 in /opt/conda/lib/python3.7/site-packages (from ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (41.0.1)
Requirement already satisfied: pickleshare in /opt/conda/lib/python3.7/site-packages (from ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (0.7.5)
Requirement already satisfied: prompt-toolkit<2.1.0,>=2.0.0 in /opt/conda/lib/python3.7/site-packages (from ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (2.0.9)
Requirement already satisfied: pexpect; sys_platform != "win32" in /opt/conda/lib/python3.7/site-packages (from ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (4.7.0)
Requirement already satisfied: attrs>=17.4.0 in /opt/conda/lib/python3.7/site-packages (from jsonschema!=2.5.0,>=2.4->nbformat->notebook>=5.5.0->nbgitpuller) (19.1.0)
Requirement already satisfied: pyrsistent>=0.14.0 in /opt/conda/lib/python3.7/site-packages (from jsonschema!=2.5.0,>=2.4->nbformat->notebook>=5.5.0->nbgitpuller) (0.15.2)
Requirement already satisfied: parso>=0.3.0 in /opt/conda/lib/python3.7/site-packages (from jedi>=0.10->ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (0.4.0)
Requirement already satisfied: wcwidth in /opt/conda/lib/python3.7/site-packages (from prompt-toolkit<2.1.0,>=2.0.0->ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (0.1.7)
Requirement already satisfied: ptyprocess>=0.5 in /opt/conda/lib/python3.7/site-packages (from pexpect; sys_platform != "win32"->ipython>=5.0.0->ipykernel->notebook>=5.5.0->nbgitpuller) (0.6.0)
Installing collected packages: nbgitpuller
Successfully installed nbgitpuller-0.6.1
Removing intermediate container 8efcd491b74c
 ---> 72094eb706d6
Successfully built 72094eb706d6
Successfully tagged jupyter/enki-scipy-notebook:latest
```
Output of "docker image ls":
```
REPOSITORY                                                         TAG                 IMAGE ID            CREATED             SIZE
registry.gitlab.com/enki-portal/thermoengine/enki-scipy-notebook   latest              72094eb706d6        3 hours ago         4.18GB
msghiorso/cheers2019                                               latest              754f653e50bc        10 hours ago        3.98MB
<none>                                                             <none>              55e338e2875a        10 hours ago        425MB
golang                                                             1.11-alpine         538699c669e2        13 days ago         311MB
jupyter/scipy-notebook                                             latest              422d367c9f0d        7 weeks ago         3.69GB
```
Then execute:
```
docker push registry.gitlab.com/enki-portal/thermoengine/enki-scipy-notebook
```
```
Output:
The push refers to repository [registry.gitlab.com/enki-portal/thermoengine/enki-scipy-notebook]
2b526c37840a: Pushed 
2012f340d51f: Pushed 
8a783551ec9d: Pushed 
e7264f8c4c3f: Pushed 
218d677a9ee2: Pushed 
06208aaa2648: Pushed 
04ad8f85b24b: Pushed 
e07a73d2d55c: Pushed 
f2b73c8b3ab9: Pushed 
efb534321d5d: Pushed 
562575cce404: Pushed 
7a5862a3c5f7: Pushed 
a61c4be4ac09: Pushed 
73030e9b1a11: Pushed 
5ad4fed22172: Pushed 
3434535a093c: Pushed 
64d4577b334a: Pushed 
ecdfdf08813c: Pushed 
e1f37515a2ef: Pushed 
69ff1caa4c1a: Pushed 
e9804e687894: Pushed 
e8482936e318: Pushed 
059ad60bcacf: Pushed 
8db5f072feec: Pushed 
67885e448177: Pushed 
ec75999a0cb1: Pushed 
65bdd50ee76a: Pushed 
latest: digest: sha256:9c9150fe32900aba4222ec98ce3076c8ebe1a36a5e97a2804404d331b4b72d29 size: 5973
```
Now, the container with the image shows up in the Registry on GitLab

#### Gitlab Deploy token for container image
Username at login: gitlab+deploy-token-79194
Password: fMGxUUuc9uxRTnUsB5yw
(Name of token): ENKI_registry_read

#### Gitlab Deploy token for repository read
Username at login: gitlab+deploy-token-79705
Password: GmDNt-Gn4Ns-LAjgtgM8
(Name of token): ENKI_repository_read

## GitLab authorization
#### General
All OAuthenticators require setting a callback URL, client ID, and client secret. You will generally get these when you register your OAuth application with your OAuth provider. Provider-specific details are available in sections below. When registering your oauth application with your provider, you will probably need to specify a callback URL. The callback URL should look like:  
```
http[s]://[your-host]/hub/oauth_callback
```

#### Gitlab specific
First, you'll need to create a GitLab OAuth application (https://docs.gitlab.com/ce/integration/oauth_provider.html).

Then, add the following to your jupyterhub_config.py file:
```
from oauthenticator.gitlab import GitLabOAuthenticator
c.JupyterHub.authenticator_class = GitLabOAuthenticator
```
You can also use LocalGitLabOAuthenticator to map GitLab accounts onto local users.

You can use your own GitLab CE/EE instance by setting the GITLAB_HOST environment flag.

In the yaml fie
```
auth:
  type: github
  github:
    clientId: "y0urg1thubc1ient1d"
    clientSecret: "an0ther1ongs3cretstr1ng"
    callbackUrl: "http://<your_jupyterhub_host>/hub/oauth_callback"
```