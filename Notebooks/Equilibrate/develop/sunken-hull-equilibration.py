# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Development of Sunken Hull Method for finding min energy assemblage
# Required Python packages/modules

# +
import numpy as np
from os import path
import pandas as pd
import scipy.optimize as opt
from scipy import optimize
import scipy.linalg as lin
import scipy as sp
import sys
import sympy as sym

from collections import OrderedDict as odict

import matplotlib.pyplot as plt

import sunkenhull as hull
# -



# Required ENKI modules (ignore the error message from Rubicon running under Python 3.6+)

from thermoengine import coder, core, phases, model, equilibrate





def get_subsolidus_phases(database='Berman'):
    remove_phases = ['Liq','H2O']

    modelDB = model.Database(database)
    if database=='Stixrude':
        pure_soln_endmems = [
            'An', 'Ab', 'Spl', 'Hc', 'Fo', 'Fa', 'MgWds', 'FeWds', 'MgRwd',
            'FeRwd', 'En', 'Fs', 'MgTs', 'oDi', 'Di', 'Hd', 'cEn',
            'CaTs', 'Jd', 'hpcEn', 'hpcFs',  'MgAki', 'FeAki', 'AlAki', 'Prp',
            'Alm', 'Grs', 'Maj', 'NaMaj', 'MgPrv', 'FePrv',
            'AlPrv', 'MgPpv', 'FePpv', 'AlPpv', 'Per', 'Wus', 'MgCf', 'FeCf',
            'NaCf']

        # soln_keys_Stixrude = ['Fsp', 'Ol', 'Wds', 'Rwd', 'PrvS', 'PpvS', 'Opx',
        #                   'Cpx', 'hpCpx', 'AkiS', 'Grt', 'Fp', 'CfS', 'SplS']
        # pure_keys_Stixrude = ['CaPrv','Qz', 'Coe', 'Sti', 'Seif', 'Ky', 'Nph']
    elif database=='Berman':
        pure_soln_endmems = []

    else:
        assert False, [
            'Need to define list of pure solution endmembers to be removed '+
            'from the system, to avoid double counting.']


    phases = modelDB.phases
    [phases.pop(phs) for phs in remove_phases]
    [phases.pop(phs) for phs in pure_soln_endmems]

    return phases





# ### T,P, parameters and options for pseudo-phase generation

T = 1100.0                  # K
P = 300000.0                 # bars
# P = 1.0                 # bars

# database='Berman'
database='Stixrude'



# +
phases = get_subsolidus_phases(database=database)
# phs_sym, endmem_ids, endmem_names, mu_endmem, comp_endmem, sys_elems, phase_comps, phase_endmems = (
phs_sym, endmem_ids, endmem_names, mu_endmem, comp_endmem, sys_elems, phase_endmems = (
    hull.system_energy_landscape(T, P, phases, prune_polymorphs=False))
# display(phs_sym, endmem_ids, mu, elem_comps, sys_elems)
Nelems = len(sys_elems)
Npts = mu_endmem.size

# Determine how many phases are pure
Npure = np.where(endmem_ids[::-1]>0)[0][0]
ind_soln = np.arange(mu_endmem.size-Npure)
ind_pure = mu_endmem.size-Npure + np.arange(Npure)
# -


# +
# phase_endmems
# -



display(endmem_names)
# display(phase_comps)
# display(phase_mu_endmem)




display(sys_elems)
display(mu_endmem)


# # Define bulk composition

wt = np.random.rand(comp_endmem.shape[0])
wt = wt/np.sum(wt)
bulk_comp = np.dot(wt, comp_endmem)
# bulk_comp = np.array([0.59760393, 0.01614512, 0.04809849, 0.09232406, 0.1571301 ,
#        0.03304928, 0.05565109])

bulk_comp

# +
# T, P = 1100.0, 1.0
# bulk_comp = np.array([0.59335532, 0.00968201, 0.09360127, 0.07404719, 0.15452804,
#                       0.01331886, 0.06146731])
# -



print('T = ', T)
print('P = ', P)
print('bulk_comp = ', bulk_comp)
print('sys_elems = ', sys_elems)



# # Intitalize endmember assemblage

# * Improved initialization compared to deCapitani
# * Reasonable method of identifying the phase endmembers that match the bulk composition and minimize the Gibbs energy
# * do not consider intermediate compositions at all
# * NOTE: should perhaps include all valid species, not just compositional endmembers
#     * TODO: add species

# +
# wt_bulk, mu_bulk, ind_assem = hull.min_energy_linear_assemblage(bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5)
chempot, mu_bulk, wt_bulk, ind_assem = hull.min_energy_linear_assemblage(
    bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5, ignore_oxy=True)


# +
# # %%timeit
# # wt_bulk, mu_bulk, ind_assem = hull.min_energy_linear_assemblage(bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5)
# chempot, mu_bulk, wt_bulk, ind_assem = hull.min_energy_linear_assemblage(
#     bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5, ignore_oxy=True)
# +
# # %%timeit
# # default bounds are good: (0, None) 
# output = opt.linprog(mu_endmem, A_eq=comp_endmem[:,1:].T, b_eq=bulk_comp[1:])

# +
# Checkout PuLP library for faster solutions
# Checkout manual theriak solve
# -







# +

print('ind_assem = ', ind_assem)
print('phs_sym assemblage = ', phs_sym[ind_assem])
print('phs_endmem_ids = ', endmem_ids[ind_assem])
print('phs_endmem_names = ', endmem_names[ind_assem])
print('chempot = ', chempot)
# -

dmu = mu_endmem-np.dot(chempot, comp_endmem.T)
print('dmu(assem) = ', dmu[ind_assem])
print('dmu(non-assem) = ', dmu[wt_bulk<1e-6])



phase_endmems['PrvS']



phases_assem = phases.copy()
phase_endmems_assem = phase_endmems.copy()
# need deep copy



# ## Update Affinities

# * Similar to nonlinear minimization  of deCapitani (Gradient descent)
# *



def get_stable_compounds_LEGACY(phase_props, phases, Aff_thresh=1):
    comp_compounds = []
    Aff_compounds = []
    name_compounds = []
    phase_compounds = []
    
    for iphs_name in phase_props:
        iprop = phase_props[iphs_name]
        iphs = phases[iphs_name]
        
        iAff = iprop['Aff']
        
        if iAff<Aff_thresh:
            iendmem_num = iphs.endmember_num
            iendmem_comp = iprop['endmem_comp']
            icomp = iprop['comp']
            iAff_endmem = iprop['Aff_endmem']
            iendmem_names = iprop['endmem_names']
            
            phase_compounds.append(iphs_name)
            name_compounds.append(iphs_name)
            comp_compounds.append(icomp)
            Aff_compounds.append(iAff)
            
            if iendmem_num>1:
                phase_compounds.append(np.tile(iphs_name, iendmem_num))
                name_compounds.append(iendmem_names)
                comp_compounds.append(iendmem_comp)
                Aff_compounds.append(iAff_endmem)
            
            
            # print(iphs_name, ': ', iAff)
    
    phase_compounds = np.hstack(phase_compounds)
    name_compounds = np.hstack(name_compounds)
    comp_compounds = np.vstack(comp_compounds)
    Aff_compounds = np.hstack(Aff_compounds)
    
    return Aff_compounds, comp_compounds, phase_compounds, name_compounds
    

phase_endmems['Fsp']



def get_endmem_compounds(chempot, phase_props, phases):
    phase_compounds = []
    name_compounds = []
    Aff_compounds = []
    mu_compounds = []
    X_compounds = []
    comp_compounds = []
    
    phase_props['chempot'] = chempot
    for iphs_name in phase_props:
        if iphs_name=='chempot':
            continue
        iprop = phase_props[iphs_name]
        iphs = phases[iphs_name]
        
        iendmem_num = iphs.endmember_num
        iendmem_comp = iprop['endmem_comp']
        iendmem_mu = iprop['endmem_mu']
        iendmem_names = iprop['endmem_names']
        iendmem_Aff = iendmem_mu - np.dot(chempot, iendmem_comp.T)
        iendmem_X = np.eye(iendmem_num)
            
        iprop['endmem_num'] = iendmem_num
        iprop['X'] = iendmem_X
        iprop['Aff'] = iendmem_Aff
        iprop['comp'] = iendmem_comp
        iprop['mu'] = iendmem_mu
        
    return phase_props
    





phase_props_assem = get_endmem_compounds(chempot, phase_endmems, phases)

phase_props_assem['Fsp']

phase_props_assem['chempot']


def get_stable_compounds(phase_props, Aff_thresh=1):
    comp_compounds = []
    Aff_compounds = []
    mu_compounds = []
    phase_compounds = []
    
    for iphs_name in phase_props:
        if iphs_name=='chempot':
            continue
        iprop = phase_props[iphs_name]
        # iphs = phases[iphs_name]
        
        iAff = iprop['Aff']
        imu = iprop['mu']
        iNcompound = len(iAff)
        
        if np.any(iAff<Aff_thresh):
            iphs_compound = np.tile(iphs_name, iNcompound)
            icomp = iprop['comp']
            
            mu_compounds.append(imu)
            Aff_compounds.append(iAff)
            comp_compounds.append(icomp)
            phase_compounds.append(iphs_compound)
            
    phase_compounds = np.hstack(phase_compounds)
    comp_compounds = np.vstack(comp_compounds)
    Aff_compounds = np.hstack(Aff_compounds)
    mu_compounds = np.hstack(mu_compounds)
    
    return Aff_compounds, mu_compounds, comp_compounds, phase_compounds
    


verbose = True
correct_aff=True
debug=True
Niter = 5

phases_assem['Cpx'].endmember_names

for i in range(Niter):
    print('==============')
    print(i)
    print('---')
    if verbose:
        print('chempot = ', chempot)
    phase_props_assem, comp_soln, X_soln, Aff_soln, phase_names_soln, extras = (
        hull.update_phase_affinities(chempot, T, P, phases_assem, phase_props_assem, 
                                     verbose=verbose, correct_aff=correct_aff, debug=debug))
    
    Aff_compounds, mu_compounds, comp_compounds, phase_compounds = get_stable_compounds(phase_props_assem)
    
    mu_calc = np.dot(comp_compounds, chempot)
    
    if verbose:
        print(np.unique(phase_compounds))
        print('Aff_compounds = ')
        print(np.array([(iphs, iAff) for (iphs, iAff) in zip(phase_compounds, Aff_compounds)]))
        print('dmu_calc = ', mu_calc-mu_compounds)
    
    
    
    dchempot, dmu_bulk, wt_bulk, ind_assem = hull.min_energy_linear_assemblage(
        bulk_comp, comp_compounds, Aff_compounds, TOLmu=10, TOL=1e-5, ignore_oxy=True)
    
    
    phase_names_assem = np.unique(phase_compounds[wt_bulk>1e-6])
    phases_assem = {iphs_name:phases[iphs_name] for iphs_name in phase_names_assem}
    # print('chempot = ', chempot)
    # print('chempot2 = ', phase_props_assem['chempot'])
    phase_props_assem = {iphs_name:phase_props_assem[iphs_name] for iphs_name in phase_names_assem}
    phase_props_assem['chempot']= chempot.copy()
    
    print('phase_names_assem = ', phase_names_assem)
    print('dmu = ', dmu_bulk)
    
    
    chempot += dchempot
    
    mu_tot = np.dot(chempot, bulk_comp)
    print('   mu_tot = ', mu_tot)
    
    if verbose:
        print('   dchempot = ', dchempot)







# +
phase_props_assem, comp_soln, X_soln, Aff_soln, phase_names_soln, extras = (
    hull.update_phase_affinities(chempot, T, P, phases_assem, phase_props_assem))



# +

Aff_compounds, comp_compounds, phase_compounds = get_stable_compounds(phase_props_assem)
# -

print(phase_compounds)
print(np.unique(phase_compounds))

# +
# phase_props_assem['PrvS']
# -

dchempot, dmu_bulk, wt_bulk, ind_assem = hull.min_energy_linear_assemblage(
    bulk_comp, comp_compounds, Aff_compounds, TOLmu=10, TOL=1e-5, ignore_oxy=True)

# +
# ignore_oxy = True
# bulk_comp_fit = hull.reduce_rank_comp(bulk_comp, ignore_oxy=ignore_oxy)
# comp_fit = hull.reduce_rank_comp(comp_compounds, ignore_oxy=ignore_oxy)
# 
# 
# output = opt.linprog(Aff_compounds, A_eq=comp_fit.T, b_eq=bulk_comp_fit)
# output
# 
# 
# dmu_bulk = output['fun']
# # dchempot = output['']
# TOL = 1e-6
# wt_bulk = output['x']
# # wt_bulk[wt_bulk<=TOL] = 0
# ind_assem = np.where(wt_bulk>TOL)[0]
# chempot_update = hull.fit_chempot(ind_assem, bulk_comp_fit, Aff_compounds,
#                       comp_fit, ignore_oxy=True)
# 
# print(chempot_update)
# dchempot = chempot_update-chempot
# 
# print(wt_bulk)

# +



# -





# +
phase_names_assem = np.unique(phase_compounds[wt_bulk>1e-6])
phases_assem = {iphs_name:phases[iphs_name] for iphs_name in phase_names_assem}
phase_props_assem = {iphs_name:phase_props_assem[iphs_name] for iphs_name in phase_names_assem}

print('phase_names_assem = ', phase_names_assem)
print('dmu = ', dmu_bulk)

# +
chempot += dchempot

mu_tot = np.dot(chempot, bulk_comp)
print('   mu_tot = ', mu_tot)
# -





# +
# phase_endmems_assem['AkiS']
# -



for i in range(100):
    print('iter = ', i)
    phase_props, comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras = (
        hull.update_phase_affinities(chempot, T, P, phases_assem, phase_endmems_assem))
    Aff_compounds, comp_compounds, phase_compounds, name_compounds = get_stable_compounds(phase_props, phases_assem)
    
    dchempot, dmu_bulk, wt_bulk, ind_assem = hull.min_energy_linear_assemblage(
        bulk_comp, comp_compounds, Aff_compounds, TOLmu=10, TOL=1e-5, ignore_oxy=True)
    
    phase_names_assem = np.unique(phase_compounds[wt_bulk>1e-6])
    phases_assem = {iphs_name:phases[iphs_name] for iphs_name in phase_names_assem}
    phase_endmems_assem = {iphs_name:phase_endmems[iphs_name] for iphs_name in phase_names_assem}
    
    print('phase_names_assem = ', phase_names_assem)
    print('dmu = ', dmu_bulk)
    
    
    chempot += dchempot
    
    mu_tot = np.dot(chempot, bulk_comp)
    print('   mu_tot = ', mu_tot)













def get_stable_compounds(phase_props, phases, Aff_thresh=1):
    comp_compounds = []
    Aff_compounds = []
    name_compounds = []
    phase_compounds = []
    
    for iphs_name in phase_props:
        iprop = phase_props[iphs_name]
        iphs = phases[iphs_name]
        
        iAff = iprop['Aff']
        
        if iAff<Aff_thresh:
            iendmem_num = iphs.endmember_num
            iendmem_comp = iprop['endmem_comp']
            icomp = iprop['comp']
            iAff_endmem = iprop['Aff_endmem']
            iendmem_names = iprop['endmem_names']
            
            phase_compounds.append(iphs_name)
            name_compounds.append(iphs_name)
            comp_compounds.append(icomp)
            Aff_compounds.append(iAff)
            
            if iendmem_num>1:
                phase_compounds.append(np.tile(iphs_name, iendmem_num))
                name_compounds.append(iendmem_names)
                comp_compounds.append(iendmem_comp)
                Aff_compounds.append(iAff_endmem)
            
            
            # print(iphs_name, ': ', iAff)
    
    phase_compounds = np.hstack(phase_compounds)
    name_compounds = np.hstack(name_compounds)
    comp_compounds = np.vstack(comp_compounds)
    Aff_compounds = np.hstack(Aff_compounds)
    
    return Aff_compounds, comp_compounds, phase_compounds, name_compounds
    









phase_props, comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras = (
    hull.update_phase_affinities(chempot, T, P, phases_assem, phase_endmems_assem))

# +

Aff_compounds, comp_compounds, phase_compounds, name_compounds = get_stable_compounds(phase_props, phases_assem)

# +

dchempot, dmu_bulk, wt_bulk, ind_assem = hull.min_energy_linear_assemblage(
    bulk_comp, comp_compounds, Aff_compounds, TOLmu=10, TOL=1e-5, ignore_oxy=True)

phase_names_assem = np.unique(phase_compounds[wt_bulk>1e-6])
phases_assem = {iphs_name:phases[iphs_name] for iphs_name in phase_names_assem}
phase_endmems_assem = {iphs_name:phase_endmems[iphs_name] for iphs_name in phase_names_assem}


print('phase_names_assem = ', phase_names_assem)
print('dmu = ', dmu_bulk)

# +

chempot += dchempot
# -







# +
# print(phase_compounds)
# print(name_compounds)

# +
# name_compounds[wt_bulk>1e-6]

# +
# phase_names_assem
# -



















# ### Select only phases with negative energies (within TOL)



MU_TOL = 1
stable_phase_names = phase_names_soln[Aff_soln < MU_TOL]
stable_phase_names



phases_stable = {key: phases[key] for key in stable_phase_names}
phase_comps_stable = {key: phase_endmems[key] for key in stable_phase_names}
phase_comps_stable

# +
for phs in stable_phase_names:
    phs_endmem = phase_endmems[phs]
    
    iendmem_comp = phs_endmem['comp']
    iendmem_mu = phs_endmem['mu']
    iendmem_names = phs_endmem['names']
    
    iX = extras['X'][phs]
    
X = extras['X']
X['Fsp']

elem_comp = extras['elem_comp']
elem_comp['Fsp']


extras.keys()

# +

chempot, mu_bulk, wt_bulk, ind_assem = hull.min_energy_linear_assemblage(
    bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5, ignore_oxy=True)
# -





phases['Cpx'].endmember_names

endmem_names_stable = np.hstack([phs.endmember_names for phs_name, phs in phases_stable.items()])
endmem_names_stable

phase_key_stable = np.hstack([np.tile(phs_name, phs.endmember_num) for phs_name, phs in phases_stable.items()])
phase_key_stable

stable_phase_names



comp_soln_stable, X_soln_stable, Aff_soln_stable, Aff_endmem_stable, phase_names_soln_stable, extras_stable = (
    hull.update_phase_affinities(chempot, T, P, phases_stable, phase_comps_stable))

# +
display(Aff_soln_stable)
display(stable_phase_names)

display(Aff_endmem_stable)
display(endmem_names_stable)
# -



# +

TOLmu = 10
dchempot, dmu_bulk_compound, wt_bulk_compound, ind_assem_compound = hull.min_energy_linear_assemblage(
    bulk_comp, comp_compound, Aff_compound, TOLmu=TOLmu, TOL=1e-5)
# -



# +
# comp_soln_stable.shape
# X_soln_stable
# Aff_soln_stable
# -



# +
endmem_offset = 1000

Aff_compound = np.hstack(
    (Aff_soln[:-Npure], Aff_endmem[ind_soln]+endmem_offset, Aff_endmem[ind_pure]))

phs_sym_compound = np.hstack(
    (phase_names_soln[:-Npure], phs_sym[ind_soln], phs_sym[ind_pure]))

endmem_ids_compound = np.hstack(
    (np.tile(-1,Nsoln), endmem_ids[ind_soln], endmem_ids[ind_pure]))

comp_compound = np.vstack(
    (comp_soln[:-Npure,:], comp_endmem))

comp_compound_fit = hull.reduce_rank_comp(comp_compound)


# -
print(phs_sym_compound)




TOLmu = 10
dchempot, dmu_bulk_compound, wt_bulk_compound, ind_assem_compound = hull.min_energy_linear_assemblage(
    bulk_comp, comp_compound, Aff_compound, TOLmu=TOLmu, TOL=1e-5)
print('dmu_bulk = ', dmu_bulk_compound)
print('dchempot = ', dchempot)
display(phs_sym_compound[ind_assem_compound])
display(endmem_ids_compound[ind_assem_compound])

chempot += dchempot











assem_phases = np.unique(phs_sym_compound[wt_bulk_compound>0])
for iphs_sym in assem_phases:
    imask_phs = phs_sym_compound==iphs_sym
    icomp_phs = comp_compound[imask_phs]
    iwt = wt_bulk_compound[imask_phs]
    iphs = phases[iphs_sym]

    iphs_endmem_comps = phase_comps[iphs_sym]

    iphs_comp = np.dot(iwt, icomp_phs)
    ioutput = np.linalg.lstsq(iphs_endmem_comps.T, iphs_comp)
    iendmem_mols = ioutput[0]

    print(iphs_comp)
    print(iendmem_mols)
    print(iphs_endmem_comps)
    print(iphs)
    print(iphs.endmember_names)

    ichempot = iphs.chem_potential(T, P, mol=iendmem_mols).squeeze()
    print(ichempot)

    print('---')

phases

wt_bulk_compound

X_soln



# +

mu_compound_assem = Aff_compound[ind_assem_compound]
comp_compound_assem_cats = comp_compound_cats[ind_assem_compound,:]
output = np.linalg.lstsq(comp_compound_assem_cats, mu_compound_assem, rcond=-1)
dchempot = output[0]

chempot = np.hstack((0,dchempot)) + chempot_prev
print(dchempot)
print(chempot)
# -





def validate_linear_assem(T, P, database='Stixrude'):
    phases = get_subsolidus_phases(database=database)
    phs_sym, endmem_ids, mu_endmem, comp_endmem, sys_elems, phase_comps = (
        hull.system_energy_landscape(T, P, phases, prune_polymorphs=False))
    # display(phs_sym, endmem_ids, mu, elem_comps, sys_elems)
    Nelems = len(sys_elems)
    Npts = mu_endmem.size

    # # Define bulk composition

    wt = np.random.rand(comp_endmem.shape[0])
    wt = wt/np.sum(wt)
    bulk_comp = np.dot(wt, comp_endmem)

    bulk_comp_cats = bulk_comp[1:]
    comp_endmem_cats = comp_endmem[:,1:]
    print(bulk_comp_cats)

    # wt_bulk, mu_bulk, ind_assem = hull.min_energy_linear_assemblage(bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5)
    wt_bulk, mu_bulk, ind_assem = hull.min_energy_linear_assemblage(
        bulk_comp_cats, comp_endmem_cats, mu_endmem, TOLmu=10, TOL=1e-5)

    print(ind_assem)
    print(phs_sym[ind_assem])
    print(endmem_ids[ind_assem])

    mu_assem = mu_endmem[ind_assem]
    comp_assem = comp_endmem[ind_assem,:]
    output = np.linalg.lstsq(comp_assem, mu_assem, rcond=-1)
    chempot = output[0]

    dmu = mu_endmem-np.dot(chempot, comp_endmem.T)
    print('dmu(assem) = ', dmu[ind_assem])
    print('dmu(non-assem) = ', dmu[wt_bulk<1e-6])



T = 400.0                  # K
P = 10*(1e6/100)          # bars
validate_linear_assem(T, P, database='Stixrude')







# +
# phase_stable, nmol0_stable, comp_endmem_stable, extras = get_init_assem(bulk_comp, comp_endmem, mu_endmem)
