
PhaseObjC - DEW Standard State properties
=========================================

Required python code to load the PhaseObjC library. The library
libphaseobjc.dylib (see build instructions in README.md) must be
locatable to the system in a standard location (by default
/usr/local/lib)

.. code:: ipython3

    import numpy as np
    from ctypes import cdll
    from ctypes import util
    from rubicon.objc import ObjCClass, objc_method
    cdll.LoadLibrary(util.find_library('phaseobjc'))




.. parsed-literal::

    <CDLL '/usr/local/lib/libphaseobjc.dylib', handle 7f93655d7e40 at 0x1083152b0>



.. code:: ipython3

    DEWFluid = ObjCClass('DEWFluid')
    obj = DEWFluid.alloc().init()
    print (obj.phaseName)
    ns = obj.numberOfSolutionSpecies()
    print ('Number of species = ', ns)


.. parsed-literal::

    DEWFluid
    Number of species =  92


Compute standard state chemical potentials of endmember components/species …
----------------------------------------------------------------------------

| The standard state Gibbs free energies of all the species known to my
  implementation of DEW are printed and stopred in a python dictionary,
  called species
| Indoividual species can be accessed by specifying
  e.g. species[‘CO2,aq’]

.. code:: ipython3

    t = 1000 # K
    p = 1000 # bars
    print("{0:>20s} {1:>15s}".format('species', 'mu0'))
    species = {}
    for i in range (0, ns):
        pure = obj.componentAtIndex_(i)
        g = pure.getGibbsFreeEnergyFromT_andP_(t, p)
        print("{0:>20s} {1:15.2f}".format(obj.nameOfSolutionSpeciesAtIndex_(i), g))
        species[obj.nameOfSolutionSpeciesAtIndex_(i)] = g


.. parsed-literal::

                 species             mu0
                   Water      -323482.97
                  CO2,aq      -578486.32
                   O2,aq      -196858.22
                   HF,aq       -93541.24
                 NaOH,aq      -484703.11
              Mg(OH)2,aq      -316145.86
                HAlO2,aq      -862231.09
                 SiO2,aq      -852668.68
                H3PO4,aq     -1321078.96
                  SO2,aq      -518724.06
                  HCl,aq      -204057.58
                  KOH,aq      -462972.14
              Ca(OH)2,aq      -447351.34
               H2CrO4,aq      -413880.22
              Mn(OH)2,aq      -125966.90
              Fe(OH)2,aq        36321.64
              Co(OH)2,aq        77078.22
                      H+            0.00
                     OH-           81.85
                   H2,aq      -113812.11
                   CO3-2      -249008.19
                   HCO3-      -527283.44
                   CO,aq      -337160.52
                      F-      -113541.24
                 NaCl,aq      -495622.66
                     Na+      -314794.69
                  NaCO3-      -708870.56
               NaHCO3,aq     -1007434.28
              NaHSiO3,aq     -1357409.84
                MgCO3,aq      -905519.12
              Mg(HSiO3)+     -1464818.62
               Mg(HCO3)+     -1113736.08
                    Mg+2      -336309.56
                   MgCl+      -562198.25
                   MgOH+      -610026.82
                MgSO4,aq     -1179318.85
                    Al+3      -198860.01
                   AlO2-      -681533.06
                  HSiO3-      -862121.50
                Si2O4,aq     -1792714.61
                  H2PO4-     -1068552.65
                  HPO4-2      -733883.27
                   PO4-3      -341564.04
                 H3P2O7-     -2130160.06
                H2P2O7-2     -1855160.01
                  H2S,aq      -199866.60
                     HS-       120134.04
                    S2-2       402510.37
                  S2O3-2      -238346.96
                  S2O4-2      -341944.14
                  S2O5-2      -545429.74
                  S2O6-2      -743452.65
                  S2O8-2     -1023411.06
                    S3-2       359008.89
                  S3O6-2      -748645.77
                    S4-2       315655.07
                  S4O6-2      -966774.45
                    S5-2       272462.31
                  S5O6-2      -780819.04
                   SO3-2      -108853.14
                   HSO3-      -518444.58
                   SO4-2      -422760.41
                   HSO4-      -742535.40
                   HSO5-      -742457.95
                     Cl-         1306.87
                      K+      -350753.92
                  KCl,aq      -471454.85
                   KSO4-     -1012529.84
                CaCO3,aq      -873604.04
               Ca(HCO3)+     -1310406.36
                 Ca(OH)+      -723837.91
                    Ca+2      -467515.04
                   CaCl+      -711237.41
                CaCl2,aq      -883294.99
                CaSO4,aq     -1302585.99
                    Cr+2       -60363.69
                    Cr+3        61619.96
                 Cr2O7-2     -1233138.73
                  CrO4-2      -433880.22
                  HCrO4-      -829027.11
                    Mn+2      -146130.60
                   MnCl+      -422437.99
                   MnO4-      -487221.12
                  MnO4-2      -207861.03
                MnSO4,aq      -984560.06
                    Fe+2        16157.94
                    Fe+3       223814.24
                   FeCl+      -213645.42
                  FeCl+2       -56584.15
                FeCl2,aq      -485890.35
                    Co+2        56914.52
                    Co+3       405037.59


.. code:: ipython3

    print (species['CO2,aq'])


.. parsed-literal::

    -578486.3235253992

